<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SMS extends Model
{

    /**
     * Set the phone number of reciever
     * @param $to
     * @return $this
     */
    public function to($to)
    {
        $this->setAttribute('from', 'WebSMS');
        $this->setAttribute('token', '8iHt7JoSXCPnqmuHX21Y');
        $this->setAttribute('to', 9841145614);
        return $this;
    }

    /**
     * Set the text to send sms
     * @param $text
     * @return $this
     */
    public function text($text)
    {
        $this->setAttribute('text', $text);
        $this->text = $text;
        return $this;
    }

    public function send()
    {
        if (env('SMS_ENABLED'))
            $request = file_get_contents('http://api.sparrowsms.com/v2/sms?' . http_build_query($this->getAttributes()));
        return true;
    }

}
