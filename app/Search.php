<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    /**
     * Perform Search based on name
     * @param $query
     * @return \Illuminate\Support\Collection
     */
    public function name($query)
    {
        $query = strtolower($query);
        $query = explode(' ', $query);
        $params = collect(['first_name' => $query[0] ?? '', 'middle_name' => ($query[2] ?? false) ? $query[1] ?? '' : '', 'last_name' => $query[2] ?? $query[1] ?? '']);
        $template = app(Template::class)->has('user');
        $params->filter()->each(function ($p, $k) use (&$template) {
            $template = $template->where(function ($query) use ($k, $p) {
                $query = $query->where("template->{$k}", 'like', "%{$p}%");
                $p = ucfirst($p);
                $query = $query->orWhere("template->{$k}", 'like', "%{$p}%");
                $p = strtoupper($p);
                $query = $query->orWhere("template->{$k}", 'like', "%{$p}%");
            });
        });
        return $template->get();
    }

    /**
     * Perform search based on citizenship no
     * @param $query
     * @return \Illuminate\Support\Collection
     */
    public function citizenship($query)
    {
        return app(Template::class)->has('user')->whereRaw('`template`->\'$."citizenship_no"\' like ?', "%{$query}%")->get();
    }

    /**
     * Perform search based on email
     * @param $query
     * @return Search|\Illuminate\Support\Collection
     */
    public function email($query)
    {
        return app(User::class)->with('template')->has('template')->whereRaw('LOWER(email) like ?', "%{$query}%")->get()->pluck("template")->filter();
    }

    /**
     * Perform search based upon mobile number
     * @param $query
     * @return Search|\Illuminate\Support\Collection
     */
    public function mobileNumber($query)
    {
        return app(User::class)->with('template')->has('template')->whereRaw('LOWER(phone_number) like ?', "%{$query}%")->get()->pluck("template")->filter();
    }

    /**
     * Perform search based on account number
     * @param $query
     * @return \Illuminate\Support\Collection
     */
    public function accountNumber($query)
    {
        $query = strtolower($query);
        return app(Template::class)->has('user')->where('template->account_number', 'like', "%{$query}%")->get();
    }
}
