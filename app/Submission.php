<?php

namespace App;

use App\Mail\SubmissionRejected;
use App\Mail\SubmissionVerified;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mail;
use App\Traits\Searchable;
class Submission extends Model
{
    use SoftDeletes, Searchable;

    public $in = 'submissions->template';

    public $hidden = ['updated_at', 'user_id', 'deleted_at', 'bank_id'];

    public $casts = [
        'submissions' => 'json'
    ];

    /**
     * One submmission belongs to one bank
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bank()
    {
        return $this->belongsTo(User::class, 'bank_id');
    }

    /**
     * One submission belongs to one user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Retrieve the name attribute
     * @return string
     */
    public function getNameAttribute()
    {
        return implode(' ', array_filter([$this->submissions['template']['first_name'] ?? '', $this->submissions['template']['middle_name'] ?? '', $this->submissions['template']['last_name'] ?? '']));
    }

    /**
     * Get qualified name
     *
     * @return void
     */
    public function getQualifiedNameAttribute(){
        return implode(' ', array_filter([$this->submissions['template']['first_name'] ?? '', $this->submissions['template']['middle_name'] ?? '', $this->submissions['template']['last_name'] ?? '']));
    }

    /**
     * Retrieve the status of submission
     * @return mixed|string
     */
    public function getStatusAttribute()
    {
        return $this->verified_at ?? false ? 'Accepted' : ($this->rejected_at ?? false ? 'Declined' : 'Pending');
    }

    /**
     * Notify accepted.
     */
    public function verified()
    {
        $bank_name = $this->bank->profile['name'] ?? ' a bank';

        $str = "Your KYC Form has been successfully verified by {$bank_name}.";

        $this->user->email && Mail::to($this->user)->send(new SubmissionVerified($str));

        $this->user->phone_number && app(SMS::class)->to($this->user->phone_number)->text($str)->send();
    }

    /**
     * Notify rejected.
     */
    public function rejected()
    {
        $bank_name = $this->bank->profile['name'] ?? ' a bank';

        $str = "Your KYC Form submitted to {$bank_name} was not verified. Please review it again.";

        $this->user->email && Mail::to($this->user)->send(new SubmissionRejected($str));

        $this->user->phone_number && app(SMS::class)->to($this->user->phone_number)->text($str)->send();
    }
}
