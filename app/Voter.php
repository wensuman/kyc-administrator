<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voter extends Model
{
    public $primaryKey = 'application_no';

    public $casts = ['application_no' => 'string'];

    public $timestamps = false;

    public $appends = ['name', 'place'];

    public $dates = ['updated_at'];

    public function getNameAttribute()
    {
        return collect([$this->first_name, $this->middle_name, $this->last_name])->filter()->implode(' ');
    }

    public function getPlaceAttribute()
    {
        return collect([$this->tole, collect([$this->district, $this->ward_no])->filter()->implode('-')])->filter()->implode(', ');
    }
}
