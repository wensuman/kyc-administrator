<?php

namespace App\Providers;

use App\Helpers\Facebook;
use Illuminate\Support\ServiceProvider;
use Route;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        env('APP_ENV') == 'local' && \DB::listen(function ($query) {
            logger($query->sql . ' -> ' . $query->time / 1000);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
