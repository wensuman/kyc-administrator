<?php

namespace App\Http\Requests;

use Dingo\Api\Http\FormRequest;

class UploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules(){
        return [
            'file'=>"required|image"
        ];
    }
    public function messages(){
        return [
            'file.required'=>'A valid file is required',
            'file.image'=>'A valid image file is required'
        ];  
    }

   
}
