<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateNewBank extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required|email|unique:users,email',
            'name'=>'required',
            'phone_number'=>'required|unique:users,phone_number',
            'password'=>'required'
        ];
    }

    public function messages(){
        return [
            'email.required'=>'Email is required',
            'name.required'=>'Name is required',
            'password.required'=>'Name is required',
        ];
    }
}
