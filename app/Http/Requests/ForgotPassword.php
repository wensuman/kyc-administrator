<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ForgotPassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone_number'=>'required|exists:users,phone_number'
        ];
    }

    /**
     * Return the appropriate message for the error occured in the request
     * 
     **/
    public function message(){
        return [
            'phone_number.exists'=>'Phone Number has not been used in this website.'
        ];
    }
}
