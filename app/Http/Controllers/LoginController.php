<?php

namespace App\Http\Controllers;

use App\History;
use App\Http\Requests\SignInRequest;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;

class LoginController extends Controller
{
    /**
     * Perform the login for the bank
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function login(Request $request)
    {
        if (str_contains($_SERVER['SERVER_NAME'], 'bank')) {
            $role = 'bank';
        }
        if (str_contains($_SERVER['SERVER_NAME'], 'app')) {
            $role = 'user';
        }
        if (str_contains($_SERVER['SERVER_NAME'], 'admin')) {
            $role = 'admin';
        }
        $user = User::where('email', $request->get("email"))->first();

        if ($user && $user->role()->where('role_id', config('role.' . $role))->first()) {
            if (auth()->attempt($request->only('email', 'password'))) {
                activity('login')->withProperties(['client_info' => $_SERVER, 'ip' => $request->ip(), 'data' => $request->except('password')])->by(auth()->user())->log('Login was Successful');
                return redirect('/');
            }
        }
        return back()->with('message', 'Login Unauthorized')->with('type', 'error');
    }

    /**
     * Return the view page for login.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function view()
    {
        if (auth()->check()) {
            return redirect('/');
        }
        return view('login');
    }

    /**
     * Log the user out
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout()
    {
        auth()->logout();
        return redirect('login');
    }

}
