<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Template;
use App\Submission;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;

class AdminView extends Controller
{
    /**
     * Handle the admin information
     * @return AdminView|\Illuminate\View\View
     */
    public function handle()
    {
        $week = collect();
        $week->put('Total banks',User::roleBank()->whereDate('created_at', '>', Carbon::today()->subWeek())->count());
        $week->put('Total users', User::roleUser()->whereDate('created_at', '>', Carbon::today()->subWeek())->withTrashed()->count());
        $week->put('Total submissions',Submission::withTrashed()->whereDate('created_at', '>', Carbon::today()->subWeek())->count());
        $week->put('Total activity',Activity::whereDate('created_at','>',Carbon::today()->subWeek())->count());

        $recent = collect();
        $recent->put('Users', User::roleUser()->with('template')->orderByDesc('created_at')->take(10)->get());
        $recent->put('Banks',User::roleBank()->orderByDesc('created_at')->take(10)->get());
        // $recent->put('Submission',Submission::with('bank','user')->orderByDesc('created_at')->take(10)->get());

        return view('Admin.view')->with('stats',collect()->put('Weekly',$week))->with('recents',$recent);
    }

    /**
     * Api for downloading forms of submissions made to the bank
     * @param Request|\Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function download(\Request $request, $id){
        $submissions = Template::where('id',$id)->firstOrFail()->template;
        $name = ($submissions['first_name'] ?? '') .' '.( $submissions['middle_name'] ?? '') .' '. ($submissions['last_name'] ?? '') .'_'. ($submissions['citizenship'] ?? $submissions['citizenship_no'] ?? '').'.pdf';
        $pdf = \PDF::loadView('Bank.submission',$submissions);
        return $pdf->download($name);
    }

    /**
     * Api for downloading forms of submissions made to the bank
     * @param Request|\Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function print(\Request $request, $id){
        $submissions = Template::where('id',$id)->firstOrFail();
        $name = ($submissions['first_name'] ?? '') .' '.( $submissions['middle_name'] ?? '') .' '. ($submissions['last_name'] ?? '') .'_'. ($submissions['citizenship'] ?? $submissions['citizenship_no'] ?? '').'.pdf';
        $pdf = \PDF::loadView('Bank.submission',$submissions);
        return $pdf->stream($name);
    }
}
