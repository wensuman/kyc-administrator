<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateProfile;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;
use App\User;

class ProfileController extends Controller
{

    /**
     * Return the profile view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(){
        return view('Admin.profile')->with('activities',Activity::where('causer_id',auth()->user()->id)->orderByDesc('created_at')->take(10)->get());
    }

    /**
     * Update the profile
     * @param UpdateProfile $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateProfile $request){
        $user = $request->user();
        
        if(User::where($request->only(['email','phone_number']))->where('id','!=',$user->id)->first()){
            return back()->with('type','error')->with('message','Either phone number or email has already been registered with the system.');
        }

        $user->email = $request->real_email;
        $user->phone_number = $request->phone_number;

        /**
         * Update the profile key
         */
        foreach($request->except(['id']) as $key => $req){
            $user->setAttribute("profile->{$key}",$req);
        }

        /**
         * Update the picture  if available
         */
        if($request->file('profile_pic')){
            $name = $request->file('profile_pic')->store('profile_pictures');
            activity('profile')->by($request->user())->on($request->user())->withProperties(['client_info'=>$_SERVER,'ip'=>$request->ip(),'data'=>$request->all()])->log('Profile  Picture was updated');
            $user->setAttribute('profile->pic',$name) ;
        }

        $user->save();

        /**
         * Log the activity
         */
        activity('profile')->by($request->user())->on($request->user())->withProperties(['client_info'=>$_SERVER,'ip'=>$request->ip(),'data'=>$request->all()])->log('Profile was updated');

        return back()->with('message','Profile was updated successfully');
    }
}
