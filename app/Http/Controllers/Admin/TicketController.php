<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ticket;

class TicketController extends Controller
{
    /**
     * Return the view for tickets
     * @param Request $request
     * @return TicketController|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Request $request)
    {
        return view('Admin.ticket')->with('tickets', Ticket::open($request->get('open') == 1)->has('user')->orderByDesc('created_at')->paginate(10));
    }

    /**
     * Update the answer in ticket
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        $ticket = new Ticket();
        $ticket = $ticket->findOrFail($request->get('id'));
        $ticket->setAttribute('ticket->answer', $request->get('answer'));
        $ticket->save();

        activity('ticket')->by(auth()->user())->on($ticket)->withProperties(['ip' => $request->ip(), 'data' => $request->all(), 'browser' => $_SERVER])->log("A new answer has been posted for question with id {$ticket->id}. ");

        return back()->with('message', ($ticket->ticket['answer'] ?? false) ? 'Answer has been modified successfully.' : 'Answer has been successfully posted.');
    }
}
