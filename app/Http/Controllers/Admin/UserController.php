<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Template;
use App\Http\Requests\AddUser;
use App\User;
use App\UserRole;
use DB;

class UserController extends Controller
{
    /**
     * Display all the mail users
     * @param null $id
     * @return UserController|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($id = null)
    {
        $users = DB::connection('mail')->table('virtual_users')->get();
        return view('Admin.users')->with('users', $users);
    }

    /**
     * Store the user both to site and mail
     * @param AddUser $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddUser $request)
    {
        $message = "User has been successfully created.";
        $type = "success";
        $user = User::forceCreate($request->only('email', 'password'));

        $user->role()->save(app(UserRole::class)->setAttribute('role_id', config('role.admin')));

        $sql = "INSERT INTO `servermail`.`virtual_users` ( `domain_id`, `password` , `email`) VALUES ( '1', ENCRYPT('{$request->password}', CONCAT('$6$', SUBSTRING(SHA(RAND()), -16))),'{$request->email}');";
        DB::connection('mail')->insert($sql);
        return back()->with('message', $message)->with('type', $type);
    }

    /**
     * Destroy the user in both places
     * @param $email
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($email)
    {
        User::where('email', $email)->forceDelete();
        DB::connection('mail')->table('virtual_users')->where('email', $email)->delete();
        return back()->with('message', 'User has been deleted successfully');
    }
}
