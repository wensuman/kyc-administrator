<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class Screening extends Controller
{
    /**
     * Update the screening activity
     * @param Request $request
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $user){

    	$user = User::findOrFail($user);
		$user->setAttribute("screening",$request->all());
    	$user->save();

    	activity('screening')->by(auth()->user())->on($user)->withProperties(['ip'=>$request->ip(),'data'=>$request->all(),'browser'=>$_SERVER])->log("Screening activity of a customer has been updated");

    	return response()->json(['data'=>['message'=>'Successfully Updated']])->setStatusCode(202);
    }
}
