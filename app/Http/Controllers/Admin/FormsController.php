<?php

namespace App\Http\Controllers\Admin;

use App\Submission;
use App\Http\Controllers\Controller;
use App\Template;
use DB;
use Illuminate\Http\Request;

class FormsController extends Controller
{
    /**
     * Return the lists of forms
     * @param Request $request
     * @param null $id
     * @return FormsController|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function lists(Request $request, $id = null)
    {
        if ($id) {
            $value = Template::with("user")->findOrFail($id);
            return view('Admin.form', $value->template)->with('submission', $value);
        }

        $column = $request->get('orderBy', 'created_at');

        $template = Template::with("user");

        switch ($request->get('filter')) {

            case 'accepted':
                $template = $template->where('verified_at','!=',NULL);
                break;

            case 'declined':
                $template = $template->where('rejected_at','!=',NULL);
                break;

            case 'pending':
                $template = $template->where('verification_requested_at','!=',NULL);
                break;
            
            default:
            $template = $template->where('verification_requested_at','!=',NULL)->orWhere('rejected_at','!=',NULL)->orWhere('verified_at','!=',NULL);
                break;

        }

        return view('Admin.forms')->with('submissions', $template->orderByDesc('updated_at')->paginate(10));
    }
}
