<?php

namespace App\Http\Controllers\Admin;

use App\Voter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDO;

class VotersExcelController extends Controller
{
    /**
     * Return the voters list upload page.
     * @param Request $request
     * @return mixed
     */
    public function view(Request $request)
    {
        return view('Admin.excel-upload')->withVoters(Voter::paginate(10))->with('voter', $request->has('application_no') ? Voter::find($request->get('application_no')) : null);
    }

    /**
     * Store and process the excel files of the users
     * @param Request $request
     * @return $this
     */
    public function upload(Request $request)
    {
        $filename = $request->file('file')->move(storage_path('excels'), str_random(8));
        $filename = $filename->getPathname();
        $sql = 'LOAD DATA LOCAL INFILE \'' . $filename . '\' REPLACE INTO TABLE voters FIELDS TERMINATED BY \',\'  ENCLOSED BY \'"\' LINES TERMINATED BY  \'\n\' (application_no,first_name,middle_name,last_name,nationality, date_of_birth_np,gender,citizenship_no, father_name,mother_name,spouse_name,district, municipality,ward_no,tole,voting_booth)';
        $db_host = config('database.connections.mysql.host');
        $db_name = config('database.connections.mysql.database');
        $db_user = config('database.connections.mysql.username');
        $db_pass = config('database.connections.mysql.password');
        $connect = new PDO("mysql:host={$db_host};dbname={$db_name}", $db_user, $db_pass, array(PDO::MYSQL_ATTR_LOCAL_INFILE => true));
        
        $result = $connect->exec($sql);
        $count = Voter::count();
        return collect()->put('data', ['message' => "File with voters data was successfully uploaded."]);
    }
}
