<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Notifications\TemplateAccepted;
use App\Notifications\TemplateRejected;
use App\Template;
use Carbon\Carbon;

class VerificationController extends Controller
{
    /**
     * Verify the user's templates
     * @param null $id
     * @return VerificationController|\Illuminate\Http\JsonResponse
     */
    public function verify($id = null)
    {
        $template = Template::findOrFail($id);

        $template->setAttribute('template->reason', request()->get('reason'));
        $template->setAttribute('verified_at', null);
        $template->setAttribute('rejected_at', null);
        $template->setAttribute('verification_requested_at', null);

        if (request()->get('verified') === 'rejected') {
            @$template->rejected();
            $template->setAttribute('rejected_at', Carbon::now());
        } else if (request()->get('verified') === 'verified') {
            @$template->accepted();
            $template->setAttribute('verified_at', Carbon::now());
        }

        $template->save();

        activity('verification')->by(auth()->user())->on($template)->withProperties(['ip' => request()->ip(), 'data' => request()->all(), 'browser' => $_SERVER])->log("KYC form has been changed to " . request()->get('verified') . ".");

        return response()->json()->setStatusCode(202);
    }
}
