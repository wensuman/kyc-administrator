<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ChangePassword;
use App\Http\Requests\MailChangePassword;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class PasswordController extends Controller
{
    /**
     * Change the password of administration
     * @param ChangePassword $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function change(ChangePassword $request){
        if(auth()->attempt(['email'=>$request->user()->email,'password'=>$request->get('old_password')])){
            $request->user()->password = $request->get('password');
            $request->user()->save();
            activity('password')->withProperties(['client_info'=>$_SERVER,'ip'=>$request->ip()])->by($request->user())->log('Password was changed');
            return back()->with('message','Password has been successfully changed');
        }
        return back()->with('message','Please, correct your old password')->with('type','error');
    }

    /**
     * Mail password change
     * @param MailChangePassword $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function mailPasswordChange(MailChangePassword $request){
        $sql = "UPDATE `servermail`.`virtual_users` SET password=ENCRYPT('{$request->password}', CONCAT('$6$', SUBSTRING(SHA(RAND()), -16))) where id={$request->get('user_id')}";
        DB::connection('mail')->update($sql);
        return back()->with('message','Password changed successfully.');
    }
}
