<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateNewBank;
use App\User;
use App\UserRole;
use Illuminate\Http\Request;

class BankController extends Controller
{
    /**
     * Store a new bank
     * @param CreateNewBank $request
     * @return BankController|\Illuminate\Http\JsonResponse
     */
    public function store(CreateNewBank $request)
    {

        $user = new User();
        $user->forceFill($request->only('email', 'password', 'phone_number'));
        $user->setAttribute('profile', $request->only(['address', 'operator', 'name']));
        $user->save();

        $user->role()->save(app(UserRole::class)->setAttribute('role_id', 2));

        activity('bank')->on($user)->by($request->user())->withProperties(['client_info' => $_SERVER, 'ip' => $request->ip(), 'data' => $request->all()])->log("{$request->get('name')} was added");

        return response()->json()->setStatusCode(201);
    }

    /**
     * Return the list of bank
     * @return mixed
     */
    public function lists()
    {
        return User::roleBank()->withTrashed()->paginate(10);
    }

    /**
     * Return the view for admin to add banks
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('Admin.bank')->with("banks", User::roleBank()->withTrashed()->paginate(5));
    }

    /**
     * Destroy the bank at any point
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy(Request $request, $id)
    {
        $bank = User::roleBank()->withTrashed()->findOrFail($id);
        $bank->forceDelete();
        activity('bank')->on($bank)->by($request->user())->withProperties(['client_info' => $_SERVER, 'ip' => $request->ip(), 'data' => $request->all()])->log("{$bank->profile['name']} is deleted");
        return \Response::make('')->setStatusCode(202);
    }

    /**
     * Disable the bank
     * @param Request $request
     * @param $id
     * @return BankController|\Illuminate\Http\Response
     */
    public function disable(Request $request, $id)
    {
        $bank = User::roleBank()->withTrashed()->findOrFail($id);
        $bank->changeStatus();
        $status = $bank->trashed() ? 'disabled' : 'enabled';
        activity('bank')->on($bank)->by($request->user())->withProperties(['client_info' => $_SERVER, 'ip' => $request->ip(), 'data' => $request->all()])->log("{$bank->profile['name']} has been {$status} ");
        return \Response::make('')->setStatusCode(202);
    }

    /**
     * Return the view for creating bank
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createView()
    {
        return view('Admin.create-bank');
    }
}
