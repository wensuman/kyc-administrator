<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Template;

class SearchController extends Controller
{
    /**
     * Return all the templates
     * @return static
     */
    public function lists()
    {
        return Template::all()->map->display()->filter();
    }

    /**
     * Return the search system
     * @param $a
     * @param $b
     * @return SearchController|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function respond($a, $b)
    {
        return view('Admin.search')->with('results', app(Template::class)->{camel_case($b)}($a));
    }
}
