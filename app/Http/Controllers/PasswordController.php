<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePassword;

class PasswordController extends Controller
{
    /**
     * Change the password of user.
     * @param ChangePassword $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function change(ChangePassword $request){
        if(auth()->attempt(['email'=>$request->user()->email,'password'=>$request->get('old_password')])){
            $user = $request->user();
            $user->password = $request->get('password');
            activity('password')->withProperties(['client_info'=>$_SERVER,'ip'=>$request->ip()])->by($request->user())->log('Password was changed');
            $user->save();
        }
        return back()->with('message','Password has been successfully changed');

    }
}
