<?php

namespace App\Http\Controllers\Bank;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    /**
     * Return the notifications view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function lists(){
		return view('Bank.notifications')->with('notifications');
    }

    /**
     * Return the notifications of the user
     * @return mixed
     */
    public function respond(){
    	return auth()->user()->notifications()->orderByDesc('created_at')->paginate(10)->each(function($notification){
    		if(($difference = $notification->created_at->diffInSeconds(\Carbon\Carbon::now())) < 60){
				$notification->timestamp = "{$difference} second ago";
				return;
    		}
    		if(($difference = $notification->created_at->diffInMinutes(\Carbon\Carbon::now())) < 60){
				$notification->timestamp = "{$difference} minute ago";
				return;
    		}
    		if(($difference = $notification->created_at->diffInHours(\Carbon\Carbon::now())) < 24){
				$notification->timestamp = "{$difference} hour ago";
				return;
    		}
    		if(($difference = $notification->created_at->diffInDays(\Carbon\Carbon::now())) < 7){
				$notification->timestamp = "{$difference} days ago";
				return;
    		}
			if(($difference = $notification->created_at->diffInWeeks(\Carbon\Carbon::now())) < 7){
							$notification->timestamp = "{$difference} week ago";
							return;
			    		}
			if(($difference = $notification->created_at->diffInMonths(\Carbon\Carbon::now())) < 12){
										$notification->timestamp = "{$difference} month ago";
										return;
						    		}
    		if(($difference = $notification->created_at->diffInYears(\Carbon\Carbon::now())) < 12){
				$notification->timestamp = "{$difference} year ago";
				return;
    		}
    	});
    }

    /**
     * Clear the unread notifications
     */
    public function clear(){
    	auth()->user()->unreadNotifications->markAsRead();
    }
}
