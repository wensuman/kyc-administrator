<?php

namespace App\Http\Controllers\Bank;

use App\Http\Controllers\Controller;
use Spatie\Activitylog\Models\Activity;

class BankView extends Controller
{
    /**
     * Return the admin view for bank
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function handle()
    {
        return view('Bank.view')->with('activities',Activity::where('causer_id',auth()->user()->id)->orderByDesc('created_at')->take(10)->get());
    }
}
