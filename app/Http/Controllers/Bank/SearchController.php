<?php

namespace App\Http\Controllers\Bank;

use App\Http\Controllers\Controller;
use App\Submission;
use App\User;
use Carbon\Carbon;
use Spatie\Activitylog\Models\Activity;

class SearchController extends Controller
{
    /**
     * Respond with submissions list for ajax
     * 
     * @return SearchController|\Illuminate\Support\Collection
     */
    public function lists()
    {
        return Submission::where("bank_id", auth()->user()->id)->get()->map(function ($map) {
            return [
                'name' => $map->qualified_name,
                'citizenship' => $map->submissions['template']["citizenship_no"] ?? "",
                'email' => $map->user->email ?? $map->submissions['template']["permanent_email"] ?? "",
                'customer_id' => $map->submissions['']["cusomter_id"] ?? "",
                'account_number' => $map->submissions['account_number'] ?? '',
                'id' => $map->id
            ];
        });
    }

    /**
     * Respond with view and search matches
     * 
     * @param $a
     * @param $b
     * 
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function respond($a, $b)
    {
        return view('Bank.search')->with('results', app(Submission::class)->{camel_case($b)}($a, Submission::class));
    }
}
