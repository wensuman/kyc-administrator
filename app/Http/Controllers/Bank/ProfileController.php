<?php

namespace App\Http\Controllers\Bank;


use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests\UpdateProfile;

class ProfileController extends Controller
{

    /**
     * Return view for displaying bank
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(){
        return view('Bank.profile');
    }

    /**
     * Update the profile of the bank
     * @param UpdateProfile $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateProfile $request){
        $user = $request->user();
         if(User::where($request->only(['email','phone_number']))->where('id','!=',$user->id)->first()){
            return back()->with('type','error')->with('message','Either phone number or email has already been registered with the system.');
        }
        foreach($request->except(['email','id']) as $key => $req){
            $user->setAttribute("profile->{$key}",$req);

        }
        if($request->file('profile_pic')){
            $name = $request->file('profile_pic')->store('profile_pictures');
            $user->setAttribute('profile->pic',$name);
            $user->profile = array_filter($user->profile);
        }
        $user->save();

        activity('profile')->by($request->user())->on($request->user())->withProperties(['client_info'=>$_SERVER,'ip'=>$request->ip(),'data'=>$request->all()])->log('Profile was updated');
        return back()->with('message','Profile was updated successfully');
    }
}
