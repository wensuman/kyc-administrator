<?php

namespace App\Http\Controllers\Bank;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Submission;
class FormsController extends Controller
{
    /**
     * Return the lists of submissions
     * @param Request $request
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function lists(Request $request, $id = null){

        $builder = Submission::with("bank")->has('user')->where('bank_id',request()->user()->id);

        if($id){

            $value = $builder->find($id);

            if(!$value){
            	return redirect('/submission');
            }

            return view('Bank.form',array_merge($value->submissions['template'] ?? [], array_except($value->submissions, ['template'])))->with('submission', $value);
        }

        $column = $request->get('orderBy', 'created_at');

        switch ($request->get('filter')) {
            
            case 'accepted':
                $builder = $builder->where('verified_at','!=',NULL);
                break;

            case 'declined':
                $builder = $builder->where('rejected_at','!=',NULL);
                break;

            case 'pending':
                $builder = $builder->where('verification_requested_at','!=',NULL);
                break;
            
            default:
                $builder = $builder->where(function($query){
                    $query->orWhere('verification_requested_at','!=',NULL)->orWhere('rejected_at','!=',NULL)->orWhere('verified_at','!=',NULL);
                });
                break;
        }

        return view('Bank.forms')->with('submissions', $builder->orderByDesc('updated_at')->paginate(10));
    }
}
