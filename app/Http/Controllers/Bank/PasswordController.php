<?php

namespace App\Http\Controllers\Bank;

use App\Http\Requests\ChangePassword;
use App\Http\Requests\ResetPassword as ResetPasswordAgain;
use App\Notifications\ResetPassword;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PasswordController extends Controller
{
    /**
     * Change the password for bank
     * @param ChangePassword $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function change(ChangePassword $request)
    {
        if (auth()->attempt(['email' => $request->user()->email, 'password' => $request->get('old_password')])) {
            $request->user()->password = $request->get('password');
            $request->user()->save();
            activity('password')->withProperties(['client_info' => $_SERVER, 'ip' => $request->ip()])->by($request->user())->log('Password was changed');
            return back()->with('message', 'Password has been successfully changed.');
        }
        return back()->with('message', 'Password could not be changed. Please correct your old password.')->with('type', 'error');
    }

    /**
     * Change the password for bank
     * @param ChangePassword|ResetPasswordAgain|Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function resetPassword(ResetPasswordAgain $request)
    {
        $user = User::where('email', $request->get('email'))->firstOrFail();
        $user->password = $request->get('password');
        $user->save();
        activity('password')->withProperties(['client_info' => $_SERVER, 'ip' => $request->ip()])->by($user)->log('Password was changed');
        return redirect(route('bank.login'))->with('message', 'Password has been successfully changed.');
    }

    /**
     * Send an email for resetting the password.
     */
    public function reset()
    {
        if (request()->has('code')) {
            if (\Cache::has(request()->get('code'))) {
                return view('forgot-password');
            }
        } else if (request()->has('email')) {
            $value = str_random(10);
            \Cache::put($value, request()->get('email', 'email'), 60);
            if ($user = User::where('email', request()->email)->first()) {
                $user->code = $value;
                $user->notify(new ResetPassword($user));
            }
            return back()->with('message', 'If exists, We have send an email with a link to reset the password.');
        }
    }
}
