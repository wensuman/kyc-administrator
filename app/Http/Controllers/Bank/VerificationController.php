<?php

namespace App\Http\Controllers\Bank;

use App\Http\Controllers\Controller;
use App\Submission;

class VerificationController extends Controller
{
    /**
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function verify($id = null)
    {
        $submission = Submission::findOrFail($id);
        $submission->setAttribute('submissions->reason', request()->get('reason'));
        $submission->setAttribute('verified_at', null);
        $submission->setAttribute('rejected_at', null);
        $submission->setAttribute('verification_requested_at', null);

        if (request()->get('verified') === 'rejected') {
            $submission->rejected();
            $submission->setAttribute('rejected_at', \Carbon\Carbon::now());
        } else if (request()->get('verified') === 'verified') {
            $submission->verified();
            $submission->setAttribute('verified_at', \Carbon\Carbon::now());
        }

        $submission->save();

        $applicant_name = 'a customer';

        activity('verification')->by(auth()->user())->on($submission)->withProperties(['ip' => request()->ip(), 'data' => request()->all(), 'browser' => $_SERVER])->log("KYC form of {$applicant_name} has been changed to " . request()->get('verified') . ".");

        return response()->json()->setStatusCode(202);
    }
}
