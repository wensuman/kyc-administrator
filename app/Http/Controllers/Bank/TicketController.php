<?php

namespace App\Http\Controllers\Bank;

use App\Ticket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TicketController extends Controller
{
    /**
     * Return the view of ticket
     * @return TicketController|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param Request $request
     */
    public function view(){
    	return view('Bank.ticket')->with('tickets',Ticket::has('user')->latest()->paginate(10));
    }

    /**
     * Store the ticket answers
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){

    	$ticket =$request->user()->issues()->findOrFail($request->get('id'));
    	$ticket->setAttribute('ticket->answer',$request->get('answer'));
    	$ticket->save();

	    activity('ticket')->by(auth()->user())->on($ticket)->withProperties(['ip'=>$request->ip(),'data'=>$request->all(),'browser'=>$_SERVER])->log("A new answer has been posted for question with id {$ticket->id}. ");
    
    	return back()->with('message',($ticket->ticket['answer'] ?? false) ? 'Answer has been modified successfully.' : 'Answer has been successfully posted.');
    }
}
