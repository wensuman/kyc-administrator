<?php

namespace App\Http\Controllers\Bank;

use App\Http\Controllers\Controller;
use App\Submission;
use Illuminate\Http\Request;
class BankController extends Controller
{

    /**
     * Return the view for viewing submissions
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function submission(Request $request, $id){

        $firstOrFail = Submission::where('id', $id)->where('bank_id', $request->user()->id)->firstOrFail();

        $submissions = $firstOrFail->submissions;

        activity('kyc')->withProperties(['client_info'=>$_SERVER,'ip'=>$request->ip(),'data'=>$request->all()])->by($request->user())->on($firstOrFail)->log("The kyc form of {$submissions['applicant_name']} was opened.");

        return \Response::view('Bank.submission',$submissions);

    }

    /**
     * Api for downloading forms of submissions made to the bank
     * @param Request|\Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function download(\Request $request, $id){

        $submissions = Submission::where('id',$id)->firstOrFail()->submissions;

        $name =implode('_', array_filter([$submissions['template']['first_name'] ?? '',$submissions['template']['middle_name'] ?? '',$submissions['template']['last_name']] ) ).'.pdf'; 

        $pdf = \PDF::loadView('Bank.submission',array_merge($submissions['template'] ?? [], $submissions));

        return $pdf->download($name);

    }

    /**
     * Api for downloading forms of submissions made to the bank
     * @param Request|\Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function print(\Request $request, $id){

        $submissions = Submission::where('id',$id)->firstOrFail()->submissions;

        $name = ($submissions['first_name'] ?? '') .' '.( $submissions['middle_name'] ?? '') .' '. ($submissions['last_name'] ?? '') .'_'. ($submissions['citizenship'] ?? $submissions['citizenship_no'] ?? '').'.pdf';

        $pdf = \PDF::loadView('Bank.submission',$submissions['template'] ?? []);

        return $pdf->stream($name);
    }
}
