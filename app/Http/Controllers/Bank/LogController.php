<?php

namespace App\Http\Controllers\Bank;


use App\Http\Controllers\Controller;
use App\Transformers\LogTransformer;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;

class LogController extends Controller
{

    /**
     * Return view for log
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Request $request){
        return view('Bank.log')->with('logs', Activity::where('causer_id',$request->user()->id)->orderByDesc('created_at')->paginate(10));
    }
}
