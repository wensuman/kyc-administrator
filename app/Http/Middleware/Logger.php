<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Validation\UnauthorizedException;

class Logger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        logger("========================{$request->fullUrl()}============================================");
        $response = $next($request);
        logger('===========================End of request=========================================');
       return $response;
    }
}
