<?php
namespace  App\Transformers;

use App\Submission;
use League\Fractal\TransformerAbstract;

class SubmissionTransformer extends TransformerAbstract{

    public function transform(Submission $submission){
        if($submission->core){
            return $submission->submissions;
        }
        $submission->setAttribute('bank_name',$submission->getRelationValue('bank')->profile['name']) ;
        return $submission->toArray();
    }

}