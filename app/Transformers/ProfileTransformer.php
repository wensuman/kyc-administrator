<?php
namespace  App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class ProfileTransformer extends TransformerAbstract{

    public function transform(User $user){
        return array_only($user->toArray(), ['id','email','phone_number','profile']);
    }

}