<?php
namespace  App\Transformers;

use Illuminate\Http\UploadedFile;
use League\Fractal\TransformerAbstract;
use Spatie\Activitylog\Models\Activity;

class UploadTransformer extends TransformerAbstract{

    public function transform(UploadedFile $file){
           return ['name'=>$file->storePublicly('uploads')];
    }

}