<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Models\Activity;

class User extends Authenticatable
{
    public $appends = ['hashed_id'];


    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone', 'password', 'another variable'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public $casts = [
        'profile' => 'json',
        'screening' => 'json'
    ];

    /**
     * Encrypt the password
     * @param $attribute
     */
    public function setPasswordAttribute($attribute)
    {
        $this->attributes['password'] = bcrypt($attribute);
    }

    /**
     * One User can have more than one role.
     */
    public function role()
    {
        return $this->hasMany(UserRole::class);
    }

    /**
     * Get the history of the user for log purpose
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function history()
    {
        return $this->hasMany(History::class);
    }

    /**
     * Get the template that belongs to user
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function template()
    {
        return $this->hasOne(Template::class);
    }

    /**
     * Get the submissions of the user
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function submissions()
    {
        return $this->hasMany(Submission::class);
    }

    /**
     * Determine if the user is bank
     * @return bool
     */
    public function isBank()
    {
        return auth()->check() && auth()->user()->role()->where('role_id', 2)->first();
    }

    /**
     * Determine if the user is admin
     * @return bool
     */
    public function isAdmin()
    {
        return auth()->check() && auth()->user()->role()->where('role_id', 3)->first();
    }

    /**
     * Determine if the user is customer or not
     * @return bool
     */
    public function isSimpleUser()
    {
        return auth()->check() && auth()->user()->role()->where('role_id', 1)->first();
    }

    /**
     * User with role of bank
     * @return mixed
     */
    public static function roleBank()
    {
        return self::whereHas('role', function ($query) {
            $query->where('role_id', '2');
        });
    }

    /**
     * User with role of customer
     * @return mixed
     */
    public static function roleUser()
    {
        return self::whereHas('role', function ($query) {
            $query->where('role_id', 1);
        });
    }

    public function changeStatus()
    {
        if ($this->trashed()) {
            $this->restore();
        } else {
            $this->delete();
        }
    }


    public function activities()
    {
        return $this->hasMany(Activity::class, 'causer_id');
    }

    public function issues()
    {
        return $this->hasMany(Ticket::class, 'bank_id');
    }

    /**
     * Generate the hashed ID attribute
     * @return bool|string
     */
    public function getHashedIdAttribute()
    {
        return substr(preg_replace('/\d+/u', '', md5($this->id)),0,3).'-'.$this->id;
    }

}
