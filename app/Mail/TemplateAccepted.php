<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TemplateAccepted extends Mailable
{
    use SerializesModels;

    public $subject = 'Update on your KYC Form';
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->markdown('mail.form', ['text' => 'Your KYC form has been successfully verified.', 'url' => env('APPI_URL') . '/form']);
    }
}
