<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubmissionVerified extends Mailable
{
    use SerializesModels;
    public $texty;
    public $subject = 'Update on your KYC Form';


    /**
     * Create a new message instance.
     * @param $text
     */
    public function __construct($text)
    {
        $this->texty = $text;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.form', ['text' => $this->texty, 'url' => env('APPI_URL')]);
    }
}
