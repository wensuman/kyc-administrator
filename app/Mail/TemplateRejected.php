<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TemplateRejected extends Mailable
{
    use SerializesModels;
    public $subject = 'Update on your KYC Form';
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->markdown('mail.form', ['text' => 'Your KYC form was not verified. Please review and submit for verification again.','url'=>env('APPI_URL').'/form/edit']);
    }
}
