<?php
namespace App\Traits;
use App\User;

trait Searchable{

    public function name($for){

        $explode = explode(' ',$for);
        $func = 'callWith'.count($explode);

        // if(method_exists($this, $func))
            return $this->{$func}($explode)->get();
        return $this->defaultName($explode);
    }

    public function callWith1($explode){

        $name = array_first($explode);

        return $this->where(function($query) use($name) {
            $cap = ucfirst($name);
            $upper  = strtoupper($name);
             $query->orWhere($this->in.'->first_name','like',"%{$name}%")->orWhere($this->in.'->first_name','like',"%{$cap}%")->orWhere($this->in.'->first_name','like',"%{$upper}%");
        });

    }

    public function callWith2($explode){
        $name = array_last($explode);

        return $this->callWith1($explode)->where(function($query) use($name) {
            $cap = ucfirst($name);
            $upper = strtoupper($name);
            $query->orWhere($this->in.'->last_name','like',"%{$name}%")->orWhere($this->in.'->last_name','like',"%{$cap}%")->orWhere($this->in.'->last_name','like',"%{$upper}%");
        });
    }

    /**
     *
     * @param [type] $explode
     * @return void
     */
    public function callWith3($explode){
        
        $name = $explode[1];

        $last = array_last($explode);

        return $this->callWith1($explode)->where(function($query) use($name) {
            $cap = ucfirst($name);
            $upper  = strtoupper($name);
            $query->orWhere($this->in.'->middle_name','like',"%{$name}%")->orWhere($this->in.'->middle_name','like',"%{$cap}%")->orWhere($this->in.'->middle_name','like',"%{$upper}%");
        })->where(function($query) use($last) {
            $cap = ucfirst($last);
            $upper  = strtoupper($last);
            $query->orWhere($this->in.'->last_name','like',"%{$last}%")->orWhere($this->in.'->last_name','like',"%{$cap}%")->orWhere($this->in.'->last_name','like',"%{$upper}%");
        });
    }

    /**
     * Search by citizenship number
     *
     * @param string $for
     * 
     * @return void
     * 
     */
    public function citizenship($for){
        return $this->where($this->in.'->citizenship_no','like',"%{$for}%")->get();
    }

    /**
     * Search by email
     *
     * @return void
     * 
     */
    public function email($for){
        return $this->where(function($query)  use ($for) {
            $query->orWhere($this->in.'->email','like',"%{$for}%")->orWhereHas('user',function($query) use ($for){
                $query->where('email','like',"%{$for}%");
            });
        })->get();
    }

    /**
     * Search by mobile number
     *
     * @param string $for
     * 
     * @return void
     * 
     */
    public function mobileNumber($for){
        return User::with('template')->has('template')->where('phone_number','like',"%{$for}%")->get()->pluck('template');
    }
}