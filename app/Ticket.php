<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public $casts = [
        'ticket' => 'json'
    ];

    /**
     * One ticket may belong to one bank
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bank()
    {
        return $this->belongsTo(User::class, 'bank_id');
    }

    /**
     * One ticket should belong to one user.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Determine if ticket is open
     * @param $bool
     * @return $this
     */
    public static function open($bool)
    {
        return static::where('ticket->answer', null);
    }

    /**
     * Determine if the ticket is closed
     * @return $this
     */
    public function closed()
    {
        return static::where('ticket', '!=', null);
    }

}
