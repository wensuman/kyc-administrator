<?php

namespace App;

use App\Mail\TemplateAccepted;
use App\Mail\TemplateRejected;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mail;
use App\Traits\Searchable;

class Template extends Model
{
    use SoftDeletes,Searchable;

    public $in = 'template';

    public $casts = [
        'template' => 'json'
    ];

    /**
     * Formatted display for transformer
     * @return array
     */
    public function display()
    {
        return [
            'id' => $this->id,
            'name' => implode(' ', array_filter([$this->template['first_name'] ?? '', $this->template['middle_name'] ?? '', $this->template['last_name'] ?? ''])),
            'citizenship' => $this->template['citizenship_no'] ?? '',
            'mobile_number' => $this->user->email ?? $this->template['communication_email'] ?? '',
            'email' => $this->user->email ?? $this->template['communication_email'] ?? '',
        ];
    }

    /**
     * Return the user who belongs to this template
     *
     * @return \App\User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Return the status attribute
     *
     * @return String
     */
    public function getStatusAttribute()
    {
        return $this->verified_at ?? false ? 'Accepted' : ($this->rejected_at ?? false ? 'Declined' : 'Pending');
    }

    /**
     * Notify about acceptance
     *
     * @return void
     */
    public function accepted()
    {
        if (!$this->user)
            return;
        
        $this->user->email && Mail::to($this->user)->send(new TemplateAccepted());
        $this->user->phone_number && app(SMS::class)->to($this->user->phone_number)->text('Your KYC Form has been successfully verified. Thank You, KYC NEPAL')->send();
        
        return true;
    }

    /**
     * Another searching facility
     *
     * @return void
     */
    public function rejected()
    {
        if (!$this->user)
            return false;
        
        $this->user->email && Mail::to($this->user)->send(new TemplateRejected());
        $this->user->phone_number && app(SMS::class)->to($this->user->phone_number)->text('Your KYC Form could not be verified. Please review it again. Thank You, KYC NEPAL.')->send();
        
        return true;
    }

    /**
     * Still searching facility
     *
     * @return void
     */
    public function getNameAttribute(){

        return collect([$this->template['first_name'] ?? '',$this->template['middle_name'] ?? '',$this->template['last_name'] ?? ''])->filter()->implode(' ');
    }


}
