<?php

return [
    'fields' => [
        'sign-up' => [
            'email',
            'phone_number',
            'password'
        ],
        'sign-in'=>[
            'phone_number',
            'password'
        ]
    ],
    'messages' => [
        'unauthorized' => 'Either email or password is incorrect'
    ],
    'history-type'=>[
        'login'=>11,
        'login-failed'=>10
    ]
];
