<?php

use Illuminate\Database\Seeder;

class UserTableSeedr extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->setAttribute('email','dhakal@wensolutions.com');
        $user->setAttribute('password', 'password');
        $user->setAttribute('phone_number', '9841145812');
        $user->save();
        $user->role()->save((new \App\UserRole())->setAttribute('role_id',3));
       
    }
}
