<?php
Route::group(['middleware' => 'logger'], function () {
    Route::group(['middleware' => []], function () {
        $bank = 'bank.kycnepal.com.np';
        $admin = 'admin.kycnepal.com.np';

        if(env('APP_ENV') === 'local'){
            $bank = 'bank.kyc.dev';
            $admin = 'admin.kyc.dev';
        } elseif(env('APP_ENV') === 'development') {
            $bank = 'dev-bank.kycnpl.com';
            $admin = 'dev-admin.kycnpl.com';
        }

        Route::group(['domain' => $bank], function () {
            Route::get('login', 'LoginController@view')->name('bank.login-view');
            Route::post('login', 'LoginController@login')->name('bank.login');
            Route::group(['middleware' => 'bank', 'namespace' => 'Bank'], function () {
                Route::get('/', 'BankView@handle')->name('main');
                Route::get('search-template', 'SearchController@lists')->name('form-search');
                Route::get('template/{form_id}', 'BankController@view')->name('bank.view.form');
                Route::get('logs', 'LogController@view')->name('bank.logs');
                Route::get('profile', 'ProfileController@view')->name('bank.profile');
                Route::post('profile', 'ProfileController@update')->name('bank.update.profile');
                Route::resource('setting', 'SettingController');
                Route::get('submissions/{id}', 'FormsController@lists')->name('bank.view.submissions');
                Route::post('change-password', 'PasswordController@change');
                Route::get('submissions/{id}/pdf', 'BankController@print');
                Route::get('submissions/{id}/download', 'BankController@download');
                Route::get('tickets', 'TicketController@view')->name('bank.tickets');
                Route::post('tickets', 'TicketController@store')->name('bank.tickets');
                Route::post('verify-submission/{id}', 'VerificationController@verify')->name('admin.verify.template');
                Route::get('submission/{id?}', 'FormsController@lists')->name('bank.forms');
                // Route::get('notifications', 'NotificationController@lists')->name('bank.notifications');
                // Route::get('notifications-ajax', 'NotificationController@respond');
                // Route::get('clear-notifications', 'NotificationController@clear');
                Route::get('search/{query}/{filter}', 'SearchController@respond');
            });
        });
        Route::group(['domain' => $admin], function () {
            Route::get('login', 'LoginController@view')->name('admin.login-view');
            Route::post('login', 'LoginController@login')->name('admin.login');
            Route::group(['middleware' => 'admin', 'namespace' => 'Admin'], function () {
                Route::get('search-template', 'SearchController@lists')->name('admin.form-search');
                Route::get('/', 'AdminView@handle')->name('main');
                Route::resource('administrators', 'UserController');
                Route::post('update-screening/{user_id}', 'Screening@update');
                Route::get('banks', 'BankController@lists')->name('admin.bank-list');
                Route::get('create-new-bank', 'BankController@createView')->name('admin.create.bank');
                Route::resource('bank', 'BankController');
                Route::get('logs', 'LogController@view')->name('admin.logs');
                Route::get('profile', 'ProfileController@view')->name('admin.profile');
                Route::post('profile', 'ProfileController@update')->name('admin.update.profile');
                Route::patch('disable/{bank_id?}', 'BankController@disable')->name('admin.disable.bank');
                Route::resource('setting', 'SettingController');
                Route::post('change-password', 'PasswordController@change');
                Route::get('template/{id?}', 'FormsController@lists')->name('admin.forms');
                Route::get('template/{id}/pdf', 'AdminView@print');
                Route::get('template/{id}/download', 'AdminView@download');
                Route::post('tickets', 'TicketController@store')->name('admin.tickets');
                Route::get('tickets', 'TicketController@view')->name('admin.tickets');
                Route::post('verify-template/{id}', 'VerificationController@verify')->name('admin.verify.template');
                // Route::get('notifications', 'NotificationController@lists')->name('bank.notifications');
                // Route::get('notifications-ajax', 'NotificationController@respond');
                // Route::get('clear-notifications', 'NotificationController@clear');
                Route::get('search/{query}/{filter}', 'SearchController@respond');
                Route::post('mail-password-change', 'PasswordController@mailPasswordChange');
                Route::get('voters-data', 'VotersExcelController@view');
                Route::post('voters-data', 'VotersExcelController@upload')->name('admin.upload.voters-excel');
            });
        });
        Route::post('change-password', 'PasswordController@change')->name('change-password');
    });
    Route::get('logout', 'LoginController@logout')->name('logout');
    Route::resource('pdf', 'PdfTester');
    Route::get('search', 'Controller@redirect');
    Route::get('forgot-password', 'Bank\PasswordController@reset')->name('forgot-password');
    Route::post('forgot-password', 'Bank\PasswordController@resetPassword')->name('forgot-password');
});
