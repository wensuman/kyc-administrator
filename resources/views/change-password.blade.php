<form enctype="multipart/form-data" class="change-password" method="POST" action="/change-password">
        <div class="form-group label-floating ">
            <label class="control-label">Type your old password</label>
            <input autocomplete="off"  required type="password" name="old_password" class="pull-right form-control" >
        </div>
    <div class="form-group label-floating ">
        <label class="control-label">Type your new password</label>
    <input autocomplete="off"  required type="password" name="password" class="pull-right form-control" >
    
    </div>
    <div class="form-group label-floating ">
        <label class="control-label">Type your new password again</label>
        <input autocomplete="off"  required type="password" name="password_confirmation" class="pull-right form-control" >
    </div>
        <button onclick="document.getElementById("changePassword").submit()" title="Pressing this button will change the password. " type="submit" class="btn btn-primary">
        Change Password
        </button>
</form>

