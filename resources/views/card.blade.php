<div class="card administration-card">
    <div class="card-content">
        @if($header->toHtml())
            <div class="card-header">{{$header or ''}}</div>
        @endif
        <div class="card-content">
            {{$slot}}
        </div>
    </div>
</div>
