<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <!-- <link rel="apple-touch-icon" sizes="76x76" href="/assets/img/apple-icon.png" /> -->
    <!--  <link rel="icon" type="image/png" href="/assets/img/favicon.png" /> -->
    <link rel="icon" type="image/png" href="/favicon.png"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>@yield('title','KYCNEPAL')
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/assets/css/material-dashboard.css" rel="stylesheet"/>
    <link href="/assets/css/demo.css" rel="stylesheet"/>
    <link href="https://ianlunn.github.io/Hover/css/hover.css" rel="stylesheet">
    <link href="///maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" /> --}}
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons"/>
    <style>
        [v-cloak] .v-cloak--block {
            display: block;
        }

        [v-cloak] .v-cloak--inline {
            display: inline;
        }

        [v-cloak] .v-cloak--inlineBlock {
            display: inline-block;
        }

        [v-cloak] .v-cloak--hidden {
            display: none;
        }

        [v-cloak] .v-cloak--invisible {
            visibility: hidden;
        }

        .v-cloak--block,
        .v-cloak--inline,
        .v-cloak--inlineBlock {
            display: none;
        }

        .glyphicon.glyphicon-pause {
            display: none !important;
        }

        .saving-spinner i {
            margin-top: 10px;
        }

        .user-image {
            width: 44px;
            height: 44px;
        }

        .form-search a {
            color: #333;
            box-shadow: none;
            border: 1px solid rgba(109, 109, 109, 0.19);
        }

        .form-search a:hover,
        .form-search a:focus {
            border: 1px solid #f7f7f7;
            color: #9c27b0;
        }

        .active-search {
            box-shadow: none;
            border: 1px solid rgba(109, 109, 109, 0.19);
            color: #9c27b0;
        }

        #searcher1 .form-group {
            margin-top: 0 !important;
            margin-bottom: 5px;
            margin-left: 0;
            padding-bottom: 0;
            min-height: inherit;
        }

        #searcher1 .radio + .radio {
            margin-top: 0;
        }

        #searcher1 .radio {
            margin-top: 0px;
        }

    </style>
</head>
<body class="{{str_replace('.',' ',$_SERVER['SERVER_NAME'])}}">
<div class="wrapper animated fadeIn">
    <div class="sidebar" data-active-color="rose" data-background-color="black" data-image="/assets/img/sidebar-1.jpg">
        @yield('logo')
        <div class="sidebar-wrapper">
            <ul class="nav">
                <li class="{{url()->current() == url('/') ? 'active':''}}">
                    <a href="/">
                        <i class="material-icons">dashboard</i>
                        <p> Dashboard </p>
                    </a>
                </li>
                @yield('menu')
            </ul>
            <div class="navbar-minimize">
                <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                    <i class="material-icons visible-on-sidebar-regular">keyboard_arrow_left</i>
                    <i class="material-icons visible-on-sidebar-mini">keyboard_arrow_right</i>
                </button>
            </div>
        </div>
    </div>
    <div class="main-panel">
        <nav class="navbar navbar-transparent navbar-absolute">
            <div class="container hidden-xs">
                @yield('search')
            </div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
        </nav>
        <div class="content">
            <div class="container-fluid">
                @yield('contents')
            </div>
        </div>
    </div>
</div>

</body>
<!--   Core JS Files   -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="/assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/assets/js/material.min.js" type="text/javascript"></script>
<script src="/assets/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="/assets/js/material-dashboard.js"></script>

<script src="/assets/js/jquery.select-bootstrap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.2.6/vue.min.js"></script>
<script src="https://cdn.jsdelivr.net/vee-validate/2.0.0-beta.25/vee-validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.16.1/axios.min.js"></script>
<script src="https://js.pusher.com/4.0/pusher.min.js"></script>
<script defer>
    Vue.use(VeeValidate);
    axios.interceptors.response.use(function (response) {
        return response;
    }, function (error) {
        let errors = error.response.data.errors;
        for (err in errors) {
            console.log(errors[err][0])
        }
        return error;
    });


    jQuery && jQuery(document).ready(function(){
        jQuery('form').submit(function(){
            jQuery(this).find('[type=submit]').attr('disabled','disabled').html('<i class="fa fa-spin fa-spinner"></i>');
        });
    });
//    var pusher = new Pusher('3f1830721f9ac4258bbd', {
//        cluster: 'ap1'
//    });
    {{--var channel = pusher.subscribe('kyc-bank-{{auth()->user()->id}}', function (data) {--}}
{{----}}
//    });
//
//    channel.bind('Illuminate\\Notifications\\Events\\BroadcastNotificationCreated', function (data) {
//        if (data.message && data.link) {
//            PNotify.removeAll();
//            new PNotify({
//                title: '1 New Notification',
//                text: "<a href='" + data.link + "'>" + data.message + "</a>",
//                addclass: 'custom',
//                type: 'success',
//                icon: 'fa fa-file-image-o',
//                nonblock: {
//                    nonblock: true
//                }
//            });
//        }
//    });

    {{--@if(auth()->user()->isAdmin())--}}
    {{--var channel1 = pusher.subscribe('kyc-administrators-log-report', function (data) {--}}
    {{--});--}}
    {{--channel1.bind('Illuminate\\Notifications\\Events\\BroadcastNotificationCreated', function (data) {--}}
    {{--if (data.message && data.link) {--}}
    {{--PNotify.removeAll();--}}
    {{--new PNotify({--}}
    {{--title: '1 New Notification',--}}
    {{--text: "<a href='" + data.link + "'>" + data.message + "</a>",--}}
    {{--type: 'success',--}}
    {{--addclass: 'translucent custom ',--}}
    {{--icon: 'fa fa-file-image-o',--}}
    {{--nonblock: {--}}
    {{--nonblock: true--}}
    {{--}--}}
    {{--});--}}
    {{--}--}}
    {{--});--}}
    {{--@endif--}}


</script>
@include("notifier")
@if($errors->has('password'))
    <script defer>
        alert('{{ $errors->first('password') }}', 'error')
    </script>
@endif
</html>