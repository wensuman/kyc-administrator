@extends('Admin.core')
@section('nav')
    <a class="navbar-brand">Add New Bank</a>
@endsection

@section('contents')
 <div class="card administration-card">
     <div class="card-content">
        <div id="banker" v-cloak>
            <span class="v-cloak--inline">Loading...</span> <!-- Only displayed before compiling -->
            <div class="v-cloak--hidden" >
                <div class="col-sm-12">
                    <form enctype="multipart/form-data" method="POST" action="/profile">
                        <div class="row">
                            <div class="form-group label-floating is-empty ">
                                <label class="control-label">Name</label>
                                <input autocomplete="off"  data-vv-validate-on="none" v-validate="'required'" name="name" type="text"
                                       class="form-control"
                                       v-model="bank.name"
                                       required><span class="material-input"></span>
                                <span v-show="errors.has('name')" class="help is-danger" v-html="errors.first('name')"></span>
                                <span v-if="error.name && error.name[0]" v-html="error.name[0]"></span>
                            </div>
                            <div class="form-group label-floating is-empty ">
                                <label class="control-label">Contact Number</label>
                                <input autocomplete="off"  data-vv-validate-on="none" v-validate="'required'" name="phone" type="text"
                                       class="form-control"
                                       v-model="bank.phone_number"
                                       required>
                                <span class="material-input"></span>
                                <span v-show="errors.has('phone_number')" class="help is-danger"
                                      v-html="errors.first('phone_number')"></span>
                                <span v-if="error.phone_number && error.phone_number[0]" v-html="error.phone_number[0]"></span>
                            </div>
                            <div class="form-group label-floating is-empty ">
                                <label class="control-label">Email Address</label>
                                <input autocomplete="off"  data-vv-validate-on="none" v-validate="'required|email'" v-model="bank.email"
                                       class="form-control"
                                       name="email" type="text" >
                                <span class="material-input"></span>
                                <span v-show="errors.has('email')" class="help is-danger"
                                      v-html="errors.first('email')"></span>
                                <span v-if="error.email && error.email[0]" v-html="error.email[0]"></span>
                            </div>
                            <div class="form-group label-floating is-empty ">
                                <label class="control-label">Password</label>
                                <input autocomplete="off"  data-vv-validate-on="none" v-validate="'required'" name="password"
                                       type="password" class="form-control"
                                       v-model="bank.password"
                                        required><span class="material-input"></span>
                                <span v-show="errors.has('password')" class="help is-danger"
                                      v-html="errors.first('password')"></span>
                            </div>
                            <div class="form-group label-floating is-empty ">
                                <button :title="bank.name ? 'Submitting will create ' + bank.name + ' as a bank user' : 'Submitting will create a new bank user'" type="button" @click.prevent="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script')
@parent

<style>
    .disabled  td {
        color:red;
    }
</style>
<script defer>
    let model = {name: '', phone_number:'', password: '', email: ''};
    let bank = new Vue({
        el: '#banker',
        mounted: function () {
            this.get()
        },
        methods: {
            submit: function () {
                this.error = {}
                this.$validator.validateAll().then(this.post)
            },
            post: function () {
                axios.post('/bank', this.bank).then(this.success,this.error).catch(this.error)
            },
            success: function (response) {
                if(response.status && response.status === 201){
                    this.get();
                    this.bank = {name: '', password: '', email: ''}
                    alert('Bank has been successfully added.');
                }else if(response.response && (response.response.status = 422)){
                    this.error = response.response.data
                }
            },
            get: function () {
                axios.get('/bank').then(this.set).catch(this.error)
            },
            set: function (response) {
                this.banks = response.data;
            },
            error: function (error) {
                console.log(error)
                this.error = error.response.data.errors;
            },
            disable: function(bank){
              axios.patch('/bank/'+bank.id).then(this.get).catch(this.error);
            },
            destroy: function (bank) {
                var th = this;
                window.confirm('Are You sure you want to delete this bank?',function(){
                    axios.delete('/bank/' + bank.id).then(function(){
                        alert(bank.profile.name + " has been removed.")
                        window.location.reload()
                    }).catch(th.error)
                    th.banks = th.banks.filter(function (bnk) {
                        return bnk.id !== bank.id
                    })
                },function(){})
            }
        },
        data: {
            bank: model,
            banks: [],
            error:{

            }
        }
    })
</script>
@endsection