@extends('Admin.core')
@section('nav')
    <a class="navbar-brand">Banks</a>
@endsection

@section('contents')
    <div class="card administration-card">
        <div class="card-content">
            <span class="pull-right add-bank-button animated slideInRight">
                <a title="Add new bank" class="btn btn-success btn-round btn-sm hvr-grow" href="/create-new-bank">+</a></span>
            <div id="banker" v-cloak>
                <span class="v-cloak--inline">Loading...</span> <!-- Only displayed before compiling -->
                <div class="v-cloak--hidden">
                    @if($banks->count() > 0)
                        <div class="col-sm-12 no-padding table-responsive table-condensed">
                            <table class="table table-bordered table-condensed">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Contact Number</th>
                                    <th>Access Status</th>
                                    <th>Added On</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($banks as $bank)
                                    <tr class="{{$bank->deleted_at ? 'disabled' : ''}}">
                                        <td>{{($bank->profile && ($bank->profile['name'] ?? false)) ? ($bank->profile['name'] ?? ''): 'Not available'}}</td>
                                        <td>{{$bank->email}}</td>
                                        <td>{{$bank->phone_number}}</td>
                                        <td>{{$bank->deleted_at ? 'Disabled' :'Active' }}</td>
                                        <td> {{$bank->created_at->toFormattedDateString() }}</td>

                                        <td>
                                            <div class="hidden-sm hidden-xs ">
                                                <i title="Remove"
                                                   @click.prevent="destroy({{$bank->id}})"
                                                   class="ace-icon fa fa-trash-o bigger-120 hvr-grow"></i>
                                                <i @click.prevent="disable({{$bank->id}})"
                                                   title="{{$bank->deleted_at ? 'Unblock' : 'Block'}}"
                                                   class="ace-icon fa bigger-120 {{$bank->deleted_at ? 'fa-hand-peace-o' : 'fa-ban'}}"></i>
                                            </div>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{$banks->render()}}
                        </div>
                    @else
                        <div class="alert alert-primary">There are no banks associated to KYCNEPAL currently.</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script')
    @parent
    <style>
        .disabled td {
            color: red;
        }
        .card i.ace-icon.fa.fa-trash-o.bigger-120 {
            padding-right: 15px;
        }
    </style>
    <script defer>
        let model = {name: '', phone_number: '', password: '', email: ''};
        let bank = new Vue({
            el: '#banker',
            mounted: function () {
            },
            methods: {
                submit: function () {
                    this.error = {}
                    this.$validator.validateAll().then(this.post)
                },
                post: function () {
                    axios.post('/bank', this.bank).then(this.success, this.error).catch(this.error)
                },
                success: function (response) {
                    if (response.status && response.status === 201) {
                        this.get();
                        this.bank = {name: '', password: '', email: ''}
                    } else if (response.response && (response.response.status = 422)) {
                        this.error = response.response.data
                    }
                },
                get: function () {
                    window.location.reload();
                },
                set: function (response) {
                    this.banks = response.data;
                },
                error: function (error) {
                    console.log(error)
                    this.error = error.response.data.errors;
                },
                disable: function (bank) {
                    axios.patch('/disable/' + bank).then(this.get).catch(this.error);
                },
                destroy: function (bank) {
                    var th = this;
                    window.confirm('', function () {
                        axios.delete('/bank/' + bank).then(function () {
                            alert("Bank has been removed.")
                            setTimeout(function () {
                                window.location.reload()
                            }, 2000)
                        }).catch(th.error)
                        th.banks = th.banks.filter(function (bnk) {
                            return bnk.id !== bank
                        })
                    }, function () {
                    })
                }
            },
            data: {
                bank: model,
                banks: [],
                error: {}
            }
        })
    </script>
@endsection