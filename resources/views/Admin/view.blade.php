@extends('Admin.core')
@section('nav')
    <a class="navbar-brand">Dashboard</a>
@endsection

@section('contents')
    @foreach($stats as $k => $stat)
        <div class="col-lg-4 no-padding col-md-6 col-sm-6 col-xs-12">
            <div class="card administration-card ">
                <div class="card-content">
                    <div class="widget-box transparent">
                        <div class="widget-header widget-header-flat">
                            <h4 class="widget-title lighter">
                                {{$k}} Report
                            </h4>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main no-padding">
                                <table class="table table-bordered table-striped">
                                    <thead class="thin-border-bottom">
                                    <tr>
                                        <th>
                                            Category
                                        </th>
                                        <th>
                                            Status
                                        </th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($stat as $key =>$st)
                                        <tr>
                                            <td>{{$key}}</td>
                                            <td>
                                                {{$st}}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    @endforeach
    <div class="col-lg-8  col-sm-6 col-md-6 col-xs-12">
        <div class="card">
            <div class="card-content">
                <ul class="nav nav-tabs" data-tabs="tabs">
                    @foreach($recents as $key=> $recent)
                        <li class="{{$loop->first ? 'active':''}}">
                            <a data-toggle="tab" href="#{{str_slug($key)}}" aria-expanded="true">{{$key}}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($recents as $key => $recent)
                        <div id="{{str_slug($key)}}" class="tab-pane {{$loop->first ? 'active':''}}">
                            <ul class="animated fadeIn">
                                @if($recent->count() > 0)
                                    @foreach($recent->take(5) as $list)
                                        <?php $list->created_date = $list->created_at->toFormattedDateString();
                                        ?>
                                        @component('component.'.strtolower(class_basename($list)), ['list'=>$list] +array_filter($list->getAttributes()))
                                        @endcomponent
                                    @endforeach
                                @else
                                    <li>Nothing Yet</li>
                                @endif
                            </ul>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
