@extends('template')

@section('logo')
    <div class="logo">
        <a href="/" class="simple-text">
            <img src="/final_kyc_logo.png" class="img-responsive">
        </a>
    </div>
    <div class="logo logo-mini">
        <a href="/" class="simple-text">
            KYC <br/> NEPAL
        </a>
    </div>
@endsection

@section('menu')

    <li class="{{str_contains(url()->current(), '/bank') ? 'active' :'' }}">
        <a href="/bank">
            <i class="material-icons">account_balance</i>
            <p class="menu-text">Bank List </p>
        </a>
    </li>
    <li class="{{str_contains(url()->current(),'template') ? 'active' :'' }}">
        <a href="/template">
            <i class="material-icons">dvr</i>
            <p class="menu-text">Customer List</p>
        </a>
    </li>
    <li class="{{str_contains(url()->current(),'profile') ? 'active' :'' }}">
        <a href="/profile">
            <i class="material-icons">person</i>
            <p class="menu-text">Profile</p>
        </a>
    </li>
    </li>
    <li class="{{str_contains(url()->current(),'tickets') ? 'active' : ''}}">
        <a href="/tickets">
            <i class="material-icons">question_answer</i>
            <p class="menu-text">Tickets</p>
        </a>
    </li>
    <li class="{{str_contains(url()->current(),'voters-list') ? 'active' : ''}}">
        <a href="/voters-data">
            <i class="material-icons">cloud_upload</i>
            <p class="menu-text">Voter's data</p>
        </a>
    </li>
    <li class="{{str_contains(url()->current(),'administrators') ? 'active' : ''}}">
        <a href="/administrators">
            <i class="material-icons">people</i>
            <p class="menu-text">Users</p>
        </a>
    </li>
    <li>
        <a href="/logout">
            <i class="material-icons">power_settings_new</i>
            <p class="menu-text">Log Out</p>
        </a>
    </li>

@endsection


@section('search')
    <div id="searcher1" class="col-sm-10 stretchforsearch">
        <form action="/search" id="new-search-bank" v-cloak class="navbar-form navbar-right " role="search">
            <div class="form-group form-search is-empty ">
                <input autocomplete="off" name="query" @keydown.enter.prevent="search" placeholder="Search customers"
                       style="height:45px;" @keydown.esc="query=''" @keyup.enter.prevent="redirect"
                       @keydown.down.prevent="down" @keydown.up.prevent="up" type="text" class="form-control"
                       v-model="query"/>
                <span class="material-input v-cloak--hidden"></span>
                <div class="v-cloak--hidden" style="position: relative; z-index:99;width: calc( 100% );">
                    <ul class="search-dropdown v-cloak--hidden" v-if="filteredForms.length > 0">
                        <li v-for="(form,index) in filteredForms"><a :href="'/template/'+form.id" target="_blank"
                                                                     :class="tab == index ?'active-search':''"
                                                                     class="col-xs-8 v-cloak--hidden"
                                                                     style="width: 100%; " v-html='form.name'>
                            </a></li>
                        <li class="v-cloak--hidden" v-if="loaded && filteredForms.length == 0 && query"><a
                                    class="col-xs-8 " style="width: 100%;">No Results Found</a></li>
                    </ul>
                </div>
            </div>

            <select v-model="filterer" class="selectpicker  v-cloak--hidden" data-style="select-with-transition" single
                    title="Name" data-size="7">
                <option value="name">Name</option>
                <option value="citizenship">Citizenship Number</option>
                <option value="email">Email</option>
                <option value="mobile_number">Mobile Number</option>
            </select>
            <button type="submit" class="btn btn-white btn-round btn-just-icon  v-cloak--hidden">
                <i class="material-icons">search</i>
                <div class="ripple-container"></div>
            </button>
        </form>
    </div>
@endsection


@section('script')
    <script>
        const vm = new Vue({
            el: '#new-search-bank',
            mounted: function () {
                this.get();
            },
            computed: {
                filteredForms: function () {
                    const query = this.query;
                    const filterer = this.filterer;
                    if (!query) {
                        return [];
                    }
                    return this.forms.filter(function (form) {
                        return form[filterer].toLowerCase().indexOf(query.toLowerCase()) > -1;
                    }).slice(0,10);
                }
            },
            methods: {
                search: function (event) {
                    if (this.tab > -1) {
                        return;
                    }
                    if (!this.query) {
                        alert('Please Enter something to search...', 'error');
                        return;
                    }
                    event.preventDefault();
                    window.location.href = "/search/" + this.query + '/' + this.filterer;
                },
                redirect: function () {
                    if (this.tab > -1) {
                        window.open('/template/' + this.filteredForms[this.tab].id, '_blank');
                    }
                    return false;
                },
                up: function () {
                    if (!this.query) {
                        this.tab = -1;
                    }
                    if (this.tab >= 0 && this.tab <= this.filteredForms.length) {
                        this.tab--;
                    }
                    return false;
                },
                down: function () {
                    if (!this.query) {
                        this.tab = -1;
                    }
                    if (this.filteredForms.length >= 0 && this.tab !== this.filteredForms.length && (this.tab + 1) < (this.filteredForms.length)) {
                        this.tab++;
                    }
                    return false;
                },
                get: function () {
                    const th = this
                    axios.get('/search-template').then(function (response) {
                        th.forms = response.data;
                        th.loaded = true;
                    })
                },
            },
            watch: {
                filteredForms: function (val, oldVal) {
                    this.tab = -1;
                }
            },

            data: {
                query: '',
                forms: [],
                tab: -1,
                focused: false,
                filterer: 'name',
                loaded: false
            }
        });
    </script>
@endsection