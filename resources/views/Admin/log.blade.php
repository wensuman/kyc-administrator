@extends('Admin.core')
@section('nav')
    <a class="navbar-brand">Logs</a>
@endsection

@section('contents')
 <div class="card administration-card">
            <div class="card-content">
    <div >
        <div class="table-responsive" >
            <table  class="table table-bordered">
                <thead>
                <tr>
                    <th>Log Name</th>
                    <th>Description</th>
                    <th>IP address</th>
                    <th>Date & Time</th>
                </tr>
                </thead>
                <tbody>

                @foreach($logs as $log)
                    <tr>
                        <td>{{$log->log_name}}</td>
                        <td>{{$log->description}}</td>
                        <td>{{$log->properties['ip']}}</td>
                        <td>{{$log->created_at->toFormattedDateString()}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            {!! $logs->links()!!}
        </div>
    </div>
    </div>
    </div>
@endsection
