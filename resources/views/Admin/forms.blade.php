@extends('Admin.core')

@section('nav')
    <a class="navbar-brand">Customers</a>
@endsection

@section('contents')
 <script>
    var goto = function(){
        var url = '{{url()->current()}}';
        if(jQuery('[name=filter]').val())
            url = url+'?filter='+jQuery('[name=filter]').val()
        window.location.href = url;
    }
    </script>
    <div class="card administration-card">
        <div class="card-content">
                <div class="table-responsive">
                    <table id="simple-table" style="min-width: 70% !important; width: auto; margin: 0 auto;" class="table table-bordered">
                        <thead>
                        <tr>
                            <th colspan="2" title="Name of the applicant">Applicant's Name</th>
                            <th colspan="1">Submitted On</th>
                            <th colspan="1">Status: 
                            <select name="filter" onchange="goto && goto()">
                                <option  value=''>All forms</option>
                                <option @if(request()->get('filter') == 'pending') selected @endif value='pending'>Pending</option>
                                <option @if(request()->get('filter') == 'accepted') selected @endif value='accepted'>Accepted</option>
                                <option @if(request()->get('filter') == 'declined') selected @endif value='declined'>Declined</option>
                            </select>
                            </th>
                            <th colspan="3">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($submissions->isNotEmpty())
                        @foreach($submissions as $submission)
                            <tr>
                                <td colspan="2">{{ucfirst(implode(' ',array_filter([$submission->template['first_name'] ?? '',$submission->template['middle_name'] ?? '',$submission->template['last_name'] ?? '' ]))?? $submission->user->email)}}</td>
                                <td colspan="1">{{$submission->updated_at->toFormattedDateString()}}</td>
                                <td colspan="1">{{$submission->status}}</td>
                                <td colspan="3">
                                    <a href="{{url('/template/'.$submission->id)}}" class="">View
                                        Profile</a>
                                </td>
                            </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="7">
                                No {{request()->get('filter','pending')}} forms here.
                            </td>
                        <tr>
                        @endif
                        </tbody>
                        @if($submissions->isNotEmpty() && $submissions->hasPages())
                        <tfoot>
                            <tr>
                                <td colspan="7">
                                    {{$submissions->appends(request()->all())->render()}}
                                </td>
                            </tr>
                        </tfoot>
                        @endif
                    </table>
                </div>
        </div>
@endsection