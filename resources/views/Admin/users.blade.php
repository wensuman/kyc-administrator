@extends('Admin.core')

@section('contents')
<div class="administration-card">
<div class="col-sm-8 no-padding">
        <div class=" card administration-card">
            <div class="card-content">
                <div class="card-header"></div>
                <div class="card-content">
                @if($users->count() == 0)
                    <div class="alert alert-primary"> No users has been added to KYCNPL.</div>
                @else
                    <div class="table-responsive">
                        <table id="simple-table" class="table table-bordered">
                            <thead>
                            <tr>
                                <th colspan="1" title="Name of the applicant">User</th>
                                <th colspan="1">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td colspan="1">{{$user->email}}</td>
                                    <td colspan="1">
                                        <a class="strip" href="" data-toggle="modal"
                                                data-target="#change-password" data-id="{{$user->id }}" title="Change Password">
                                                <i class="material-icons" style="font-size:inherit">create</i>
                                        </a>
                                        <form style="display: inline;"  method="post" action="/administrators/{{$user->email}}">
                                            <input autocomplete="off" type="hidden" name="_method" value="delete">
                                            <button title="Delete user" type="submit" value="" class="strip red">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4 ">
        <div class="card administration-card">
                <div class="card-content">
                    <form enctype="multipart/form-data" method="POST" autocomplete="false" action="/administrators">
                        <div class="form-group">
                            <label class="label-control">Email Address</label>
                            <input autocomplete="" data-vv-validate-on="none" value="" name="email" type="text"
                                   required="required" class="form-control" aria-required="true" aria-invalid="false">
                            @if($errors->has('email'))  <span
                                    class="is-danger"> {{$errors->first('email')}}</span> @endif
                        </div>
                        <div class="form-group">
                            <label class="label-control">Password</label>
                            <input autocomplete="" data-vv-validate-on="none" value="" name="password" type="password"
                                   required="required" class="form-control" aria-required="true" aria-invalid="false">
                        </div>
                        <div class="form-group">
                            <button data-vv-validate-on="none" type="submit" required="required"
                                    class="btn btn-primary btn-block" aria-required="true" aria-invalid="false">
                                Create
                            </button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="change-password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-notice">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="material-icons">clear</i></button>
                    <h5 class="modal-title" id="myModalLabel">Change Password</h5>
                </div>
                <form id="change-password-form" action="/mail-password-change" method='post'>

                    <div class="modal-body">
                        <div class="instruction">
                            <input autocomplete="off" type="hidden" name="user_id">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating ">
                                        <label class="control-label">New Password</label>
                                        <input autocomplete="off" required="required" type="password" name="password"
                                               class="form-control " id="passwordi">
                                        <span class="material-input"></span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Confirm password</label>
                                        <input autocomplete="off" required="required" type="password"
                                               name="password_confirmation" class="form-control "
                                               id="password_confirmationr">
                                        <span class="material-input"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer text-center">
                        <button onclick="return goodToChange()" type="submit" class="btn btn-primary">Change</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection

@section('nav')
    <a class="navbar-brand">Users</a>
@endsection

@section('script')
    @parent
    <script>
        jQuery(document).ready(function () {
            jQuery('.btn-password-changer').click(function () {
                console.log(jQuery(this).attr('data-id'))
                jQuery('#change-password-form').find('input[name=user_id]').val(jQuery(this).attr('data-id'))
            });

           
        })
        var goodToChange = function (event) {
            var val1 = jQuery('#passwordi').val();
            var val2 = jQuery('#password_confirmationr').val();
            if (!val1 || !val2) {
                alert('Fill up the password fields', 'error')
            }
            var truthy = val1 == val2;
            console.log(val1)
            console.log(val2)
            console.log(truthy)
            if (!truthy) {
                alert('Passwords do not match.', 'error')
                return false;
            }
            return true;
        }
    </script>
@endsection
