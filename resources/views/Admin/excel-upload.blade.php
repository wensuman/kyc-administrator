@extends('Admin.core')

@section('contents')
    @component('card')
        @slot('header')
        @endslot
        @component('form')
            @slot('action',route('admin.upload.voters-excel'))
            <div class="col-md-6 col-sm-12">
                <legend>Upload excel file</legend>
                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                    <div>
                        <span class="btn btn-primary btn-round btn-file">
                            <span class="fileinput-new">Upload <i class="material-icons">cloud_upload</i></span>
                            <input id="exceler-file" type="file" name="excel"/>
                        </span>
                    </div>
                </div>
            </div>
        @endcomponent

        @component('form')
            @slot('method','get')
            <div class="col-md-6 col-sm-12">
                <legend>View voter's information</legend>
                <div class="col-sm-10 no-padding">
                    <div style="margin:0" class="form-group">
                        <input required placeholder="Enter voter ID" value="{{request()->get('application_no')}}"
                               class="form-control" type="text" name="application_no"/>
                    </div>
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-sm btn-primary" type="submit">
                        View
                    </button>
                </div>
                <div class="clearfix"></div>
                @if($voter)
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>Voter's ID</td>
                                <td>{{$voter->application_no}}</td>
                            </tr>
                            <tr>
                                <td>First Name</td>
                                <td>{{$voter->first_name}}</td>
                            </tr>
                            <tr>
                                <td>Middle Name</td>
                                <td>{{$voter->middle_name}}</td>
                            </tr>
                            <tr>
                                <td>Last Name</td>
                                <td>{{$voter->last_name}}</td>
                            </tr>
                            <tr>
                                <td>Nationality</td>
                                <td>{{$voter->nationality}}</td>
                            </tr>
                            <tr>
                                <td>Date of Birth (Nepali)</td>
                                <td>{{$voter->date_of_birth_np}}</td>
                            </tr>
                            <tr>
                                <td>Gender</td>
                                <td>{{$voter->gender}}</td>
                            </tr>
                            <tr>
                                <td>Citizenship number</td>
                                <td>{{$voter->citizenship_no}}</td>
                            </tr>
                            <tr>
                                <td>Father name</td>
                                <td>{{$voter->father_name}}</td>
                            </tr>
                            <tr>
                                <td>Mother name</td>
                                <td>{{$voter->mother_name}}</td>
                            </tr>
                            <tr>
                                <td>Spouse name</td>
                                <td>{{$voter->spouse_name}}</td>
                            </tr>
                            <tr>
                                <td>District</td>
                                <td>{{$voter->district}}</td>
                            </tr>
                            <tr>
                                <td>Municipality</td>
                                <td>{{$voter->municipality}}</td>
                            </tr>
                            <tr>
                                <td>Ward no</td>
                                <td>{{$voter->ward_no}}</td>
                            </tr>
                            <tr>
                                <td>Tole</td>
                                <td>{{$voter->tole}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="clearfix"></div>
                @endif
            </div>
        @endcomponent
        <div class="clearfix"></div>
    @endcomponent
@endsection

@section('script')
    @parent
    <script>
        jQuery(document).on('change', '[name=excel]', function () {

            const siblings = jQuery(this).siblings('.fileinput-new');
            siblings.attr('disabled', 'disabled').html('<i class="fa fa-spin fa-spinner"></i> Uploading <span class="percent-excel"></span>');

            const data = new FormData();
            data.append('file', document.getElementById('exceler-file').files[0]);

            const config = {
                onUploadProgress: function (progressEvent) {
                    var number = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                    number === 100 ? siblings.html('Processing...') : jQuery('.percent-excel').html(number + '%');
                }
            };

            axios.post('/voters-data', data, config).then(function (response) {
                jQuery('#exceler-file').val('');
                siblings.removeAttr('disabled').html('Upload <i class="material-icons">cloud_upload</i>');
                response.data && response.data.data && response.data.data.message && alert(response.data.data.message);
            }).catch(function (response) {
                jQuery('#exceler-file').val('');
                siblings.removeAttr('disabled').html('Upload <i class="material-icons">cloud_upload</i>');
            });
        })
        ;
    </script>
@endsection

