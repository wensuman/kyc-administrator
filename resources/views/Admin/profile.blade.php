@extends('Admin.core')

@section('nav')
    <a class="navbar-brand">Profile</a>
@endsection

@section('contents')
    <div class="administration-card">
        <div class="">
            <div class="col-md-8  col-sm-12 no-padding">
                <div class="card">
                    <div class="card-content">
                        <h4 class="card-title">Edit Profile -
                            <small class="category">Complete your profile</small>
                        </h4>
                        <form enctype="multipart/form-data" method="POST" action="/profile">
                            <div class="label-floating is-focused">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <img id="avatar" class="editable img-responsive editable-click editable-empty"
                                             alt="Alex's Avatar"
                                             src="{{ isset(auth()->user()->profile['pic']) ? asset(auth()->user()->profile['pic']) : '/profil-pic_dummy.png'}}"/>
                                        <input autocomplete="off" type="file" title="Click to change image"
                                               onchange="this.form.submit()" name="profile_pic"/>
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating is-focused">
                                            <label class="control-label">Name of Administrator</label>
                                            <input autocomplete="off" class="form-control" type="text" name="name"
                                                   value="{{isset(auth()->user()->profile['name']) ? auth()->user()->profile['name']: ''}}"/>
                                            <span class="material-input"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group label-floating is-focused">
                                            <label class="control-label">Location</label>
                                            <input autocomplete="off" class="form-control" type="text" name="location"
                                                   value="{{isset(auth()->user()->profile['location']) ? auth()->user()->profile['location'] : '' }}"/>
                                            <span class="material-input"></span></div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group label-floating is-focused">
                                            <label class="control-label">Phone Number</label>
                                            <input autocomplete="off" name="phone_number" type="text"
                                                   class="form-control" value="{{auth()->user()->phone_number}}"/>
                                            <span class="material-input"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group label-floating is-focused">
                                            <label class="control-label">Support Email </label>
                                            <input autocomplete="off" type="email" class="form-control"
                                                   name="support_email"
                                                   value="{{isset(auth()->user()->profile['support_email']) ? auth()->user()->profile['support_email'] : '' }}"/>
                                            <span class="material-input"></span></div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group label-floating is-focused">
                                            <label class="control-label">Email</label>
                                            <input autocomplete="off" type="email" class="form-control"
                                                   name="real_email" value="{{auth()->user()->email}}"/>
                                            <span class="material-input"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group label-floating is-focused">
                                        <label class="control-label">About Us</label>
                                        <textarea class="form-control" name="about" id="" cols="80"
                                                  rows="5">{{isset(auth()->user()->profile['about']) ? auth()->user()->profile['about'] : ''}}</textarea>
                                        <span class="material-input"></span></div>
                                </div>

                                <button type="submit" class="btn btn-primary pull-right" title="Update Your Profile">
                                    Update Profile
                                </button>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-12">
                <div class="card administration-card profile-box-card">
                    <div class="card-content">
                        <h4>Change Password</h4>
                        <div class="card-title"></div>
                        @include('change-password')

                    </div>
                </div>
            </div>


        </div>

    </div>




@endsection


