@extends('Admin.core')
@section('contents')
    <div class="card administration-card search-page">
        <div class="card-content">
           <div class="col-sm-12">
               <h4 class="card-title">Search for -
                   <small class="category">{{request()->route('query')}}</small>
               </h4>
           </div>
            @if($results->count() > 0)
                @foreach($results as $result)
                    <div class="col-sm-12 col-lg-6 col-md-12">
                        <div class="search-profile">
                            <div class="col-sm-4">
                                <div class="form-group">
                                @if($result->template['photo_person'] ?? false)
                                <img id="avatar" class="img-responsive" alt="{{implode(' ',array_filter([$result->template['first_name']??'',$result->template['middle_name']??'',$result->template['last_name']??'']))}}"
                                         src="{{env('API_URL').$result->template['photo_person']}}">
                                @else
                                    <img id="avatar" class="img-responsive" alt="{{implode(' ',array_filter([$result->template['first_name']??'',$result->template['middle_name']??'',$result->template['last_name']??'']))}}"
                                         src="/profil-pic_dummy.png">
                                @endif
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <h4 class="card-title">{{implode(' ',array_filter([$result->template['first_name']??'',$result->template['middle_name']??'',$result->template['last_name']??'']))}}</h4>
                                    <span class="pull-left profile-info">Citizenship Id:</span>
                                    <span class="pull-right profile-detail">{{$result->template['citizenship_no'] ?? 'Not Available'}}</span><br/>
                                    <span class="pull-left profile-info">Address:</span>
                                    <span class="pull-right profile-detail">{{ucfirst($result->template['permanent_municipality_name'] ?? $result->template['current_municipality_name'] ?? ' &nbsp;')}}</span><br/>
                                <a target="_blank" href="/template/{{$result->id}}" class=" pull-left">View
                                    more</a>
                            </div>


                        </div>
                    </div>
                @endforeach
            @else
                <div class="col-sm-12">
                    <div class="alert alert-primary">No Results Found.</div>
                </div>
            @endif
        </div>
    </div>
@endsection