<div class="card administration-card search-page">
        <div class="card-header card-header-icon" data-background-color="rose"><i class="material-icons">search</i></div>
        <div class="card-content">
        	<h4 class="card-title">Search for - <small class="category">{{request()->route('query')}}</small> 
            </h4>
            @foreach($results as $result)
            <div class="col-sm-12 col-lg-6 col-md-12">
					<div class="search-profile">
		               <div class="col-sm-4">
			                    <div class="form-group">
			                        <img id="avatar" class="img-responsive" alt="Alex's Avatar" src="/profil-pic_dummy.png">
			                     </div>
			               </div>
			               <div class="col-sm-7">
			               		<h4 class="card-title">{{implode(' ',array_filter([$result->template['first_name'] ?? '',$result->template['middle_name'] ?? '',$result->template['last_name'] ?? '']))}}</h4>
			               		<span class="pull-left profile-info">Mobile number: </span>
			               		<span class="pull-right profile-detail">{{$result->template['permanent_mobile_no'] ?? 'Not Available'}}</span><br/>
								<span class="pull-left profile-info">Citizenship Id:</span>
								<span class="pull-right profile-detail">{{$result->template['citizenship'] ?? 'Not Available'}}</span><br/>
								<span class="pull-left profile-info">Email:</span>
								<span class="pull-right profile-detail">{{$result->template['permanent_email'] ?? 'Not Available'}}</span>
								<a target="_blank" href="/template/{{$result->id}}" class="btn btn-primary pull-left">View more</a>
			                </div>

			        
			       
			            </div>
            </div>
            @endforeach
        </div>
        </div>