<div class="card">
	<div class="card-content">
	    <h4 class="card-title">Your Notifications</h4>
	    <div class="table-responsive">
	        <table class="table table-bordered">
	            <tbody>
	            <?php $notifications = auth()->user()->notifications()->orderByDesc('created_at')->paginate(10); ?>
	            @foreach($notifications->each(function($notification){
if(($difference = $notification->created_at->diffInSeconds(\Carbon\Carbon::now())) < 60){
				$notification->timestamp = "{$difference} second ago";
				return;
    		}
    		if(($difference = $notification->created_at->diffInMinutes(\Carbon\Carbon::now())) < 60){
				$notification->timestamp = "{$difference} minute ago";
				return;
    		}
    		if(($difference = $notification->created_at->diffInHours(\Carbon\Carbon::now())) < 24){
				$notification->timestamp = "{$difference} hour ago";
				return;
    		}
    		if(($difference = $notification->created_at->diffInDays(\Carbon\Carbon::now())) < 7){
				$notification->timestamp = "{$difference} days ago";
				return;
    		}
			if(($difference = $notification->created_at->diffInWeeks(\Carbon\Carbon::now())) < 7){
							$notification->timestamp = "{$difference} week ago";
							return;
			    		}
			if(($difference = $notification->created_at->diffInMonths(\Carbon\Carbon::now())) < 12){
										$notification->timestamp = "{$difference} month ago";
										return;
						    		}
    		if(($difference = $notification->created_at->diffInYears(\Carbon\Carbon::now())) < 12){
				$notification->timestamp = "{$difference} year ago";
				return;
    		}

	            	}) ?? []  as $notification)
	                <tr>
	                    <td class="{{$notification->read_at ? 'read' :'not-read'}}"><a target="_blank" href="{{$notification->data['link']}}">{{$notification->data['message']}}</td>
	                    <td>{{$notification->timestamp}}</td>
	                </tr>
	            {{$notification->markAsRead()}}
                @endforeach
	            </tbody>
	        </table>
	        {!! $notifications->render()!!}
	    </div>
	</div>
</div>