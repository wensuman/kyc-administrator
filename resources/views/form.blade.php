<form enctype="multipart/form-data" method="{{$method or 'POST'}}" action="{{$action or ''}}">
    {{$slot}}
</form>
