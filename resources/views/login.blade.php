<!DOCTYPE html>
<html lang="en">
<head>
    {{Cookie::forget('laravel_session') ? '' : ''}}
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta charset="utf-8"/>
    <title>Login Page - KYCNEPAL</title>
    <meta name="description" content="User login page"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/assets/font-awesome/4.5.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/assets/css/fonts.googleapis.com.css"/>
    <link rel="stylesheet" href="/assets/css/login.css"/>
</head>

<body class="login-layout">
<script>
    function deleteAllCookies() {
        var cookies = document.cookie.split(";");
        console.log(cookies)
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            var eqPos = cookie.indexOf("=");
            var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
        }
    }
    deleteAllCookies();
</script>
<div class="additional">
    <div class="overlay"></div>
    <div class="site-branding">
        <a href="#" class="custom-logo-link" rel="home">
            <img width="170px" src="/final_kyc_logo.png" alt="">
        </a>
    </div>
    <div class="container-page">
        <div class="container-forms">
            <div class="container-info">
            </div>
            <div class="container-form">
                <div class="form-item log-in login">
                    <div class="table">
                        <div class="table-cell">
                            <form method="post" action="{{url('login')}}">
                                {!! csrf_field() !!}
                                @if($errors->first())
                                    <p class="alert-msg">{{$errors->first()}}</p>
                                @endif
                                @if(session('message'))
                                    <p class="alert-msg">{{session('message')}}</p>
                                @endif
                                <div class="login_fields__user input-field">
                                    <div class="icon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <input required autocomplete="off"  name="email" value="" type="text" placeholder="Email"/>
                                </div>
                                <div class="login_fields__user input-field">
                                    <div class="icon">
                                        <i class="fa fa-lock"></i>
                                    </div>
                                    <input required autocomplete="off"  name="password" value="" type="password" placeholder="Password"/>
                                </div>
                                <button type="submit" class="kyc-btn login-btn login_fields__user ">
                                   Sign In
                                </button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script>
    jQuery(document).on('click','.forgotpassword', function(event){
            event.preventDefault();
            window.location.href = "{{url('forgot-password')}}"+'?email='+jQuery('input[name=email]').val();
    });
</script>
</html>



