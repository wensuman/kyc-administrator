<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.1/pnotify.css"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.1/pnotify.nonblock.css"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.1/pnotify.buttons.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.1/pnotify.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.1/pnotify.confirm.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.1/pnotify.buttons.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.1/pnotify.nonblock.js" ></script>
<script defer>
function consume_alert() {
    window.alert = function(message, type = 'success') {
        PNotify.removeAll();
        new PNotify({
            text: message,
            type: type,
            icon: false,
            styling: 'bootstrap3'
        });
    };
}

consume_alert()


function consume_confirm() {
    window.confirm = function(message, successCallback, errorCallback) {
            PNotify.removeAll();
        
        (
            new PNotify({
                title: 'Are you sure?',
                addClass:'stack-site',
                text: message,
                icon: false,
                hide: true,
                styling: 'bootstrap3',
                confirm: {
                    confirm: true
                },
                buttons: {
                    closer: false,
                    sticker: false
                },
                history: {
                    history: false
                }
            })
        ).get().on('pnotify.confirm', successCallback);
    };
}

consume_confirm()
@if(session('message'))
        new PNotify({
            text: "{{session('message')}}",
            type: "{{session('type') ?? $type ?? 'success'}}",
            styling: 'bootstrap3'
        });
@endif

</script>

@yield('script')
