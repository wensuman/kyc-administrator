@extends('template')
@section('logo')
 <div class="logo">
                <a href="/" class="simple-text">
                   <img src="/final_kyc_logo.png" class="img-responsive">
                </a>
            </div>
            <div class="logo logo-mini">
                <a href="/" class="simple-text">
                    KYC <br/> NEPAL
                </a>
            </div>
@endsection
@section('menu')
      <li class="{{str_contains(url()->current(),'submission') ? 'active' :'' }}">
        <a href="/submission">
           <i class="material-icons">dvr</i>
            <p class="menu-text">Customer List</p>
        </a>
    </li>
    <li class="{{str_contains(url()->current(),'profile') ? 'active' : ''}}">
        <a href="/profile">
            <i class="material-icons">person</i>
            <p class="menu-text">Profile</p>
        </a>
    </li>
    <li>
        <a href="/logout">
            <i class="material-icons">power_settings_new</i>
            <p class="menu-text">Log Out</p>
        </a>
    </li>
@endsection

@section('search')
<div class="col-sm-10 stretchforsearch">
    <div id="searcher1" >
        <form action="/search" id="search-for-admin" v-cloak class="navbar-form navbar-right " role="search" >
            <div class="form-group form-search is-empty">
                <input autocomplete="off"  name="query" @keydown.enter.prevent="search" placeholder="Search customers" style="height:45px;" @keydown.esc="query=''" @keyup.enter.prevent="redirect" @keydown.down.prevent="down" @keydown.up.prevent="up" type="text"   class="form-control" v-model="query"/>
                <div class="v-cloak--hidden"  style="position: relative; z-index:99;width: calc( 100% );">
                    <ul class="search-dropdown v-cloak--hidden" v-if="filteredForms.length > 0">
                        <li v-for="(form,index) in filteredForms"> <a :href="'/submissions/'+form.id" target="_blank" :class="tab == index ?'active-search':''" class="col-xs-8 v-cloak--hidden" style="width: 100%; "  v-html='form.name'>
                            </a> </li>
                        <li class="v-cloak--hidden" v-if="loaded && filteredForms.length == 0 && query"><a class="col-xs-8 " style="width: 100%;"  >No Results Found</a></li>
                    </ul>
                </div>
            </div>

            <select  v-model="filterer" class="selectpicker  v-cloak--hidden" data-style="select-with-transition" single title="Name" data-size="7">
                <option value="name">Name </option>
                <option value="citizenship">Citizenship Number</option>
                <option value="account_number">Account Number </option>
            </select>
            <button type="submit" class="btn btn-white btn-round btn-just-icon  v-cloak--hidden">
                <i class="material-icons">search</i>
                <div class="ripple-container"></div>
            </button>
        </form>
    </div>
</div>
@endsection


@section('script')
    <script>
        const vm  = new Vue({
            el:'#search-for-admin',
            mounted: function(){
                this.get();
            },
            computed:{
                filteredForms: function(){
                    const query = this.query;
                    const filterer =this.filterer;
                    if(!query){
                        return [];
                    }
                    return this.forms.filter(function(form){
                        return form[filterer].toLowerCase().indexOf(query.toLowerCase()) > -1;
                    }).slice(0,10);
                }
            },
            methods: {
                search:function(event){
                    if(this.tab > -1){
                        return;
                    }
                    if(!this.query){
                        alert('Please Enter something to search...','error');
                        return;
                    }
                    event.preventDefault();
                    window.location.href="/search/"+this.query+'/'+this.filterer;
                },
                redirect: function(){
                    if(this.tab > -1){
                        window.open( '/submissions/'+this.filteredForms[this.tab].id, '_blank' );
                    }
                    return false;
                },
                up:function(){
                    if(!this.query){
                        this.tab = -1;
                    }
                    if(this.tab >= 0 && this.tab <= this.filteredForms.length){
                        this.tab--;
                    }
                    return false;
                },
                down:function(){
                    if(!this.query){
                        this.tab = -1;
                    }
                    if(this.filteredForms.length >= 0 && this.tab !== this.filteredForms.length && (this.tab+1) < (this.filteredForms.length)){
                        this.tab++;
                    }
                    return false;
                },
                get:function(){
                    const th = this
                    axios.get('/search-template?query='+this.query).then(function(response){
                        th.forms = response.data;
                        th.loaded = true;
                    })
                },
            },
            watch: {
                filteredForms: function(val, oldVal) {
                    this.tab = -1;
                }
            },

            data:{
                query:'',
                forms:[],
                tab:-1,
                focused:false,
                filterer:'name',
                loaded:false
            }
        });
    </script>
@endsection