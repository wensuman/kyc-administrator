@extends('Bank.core')
@section('nav')
    <a class="navbar-brand">Dashboard</a>
@endsection


@section('contents')
 <div class="card administration-card middler">
    <div class="card-content">
    <div class="row">
     <div class="col-lg-3 col-md-6 col-sm-6">
         <div class="card card-stats">
             <div class="card-header" data-background-color="rose">
                 <i class="material-icons">person</i>
             </div>
             <div class="card-content">
                 <p class="category">Users</p>
                 <h3 class="card-title">{{\App\User::count()}}</h3>
             </div>
             <div class="card-footer">
                 <div class="stats">
                     Total number of  users
                 </div>
             </div>
         </div>
     </div>
     <div class="col-lg-3 col-md-6 col-sm-6">
         <div class="card card-stats">
             <div class="card-header" data-background-color="rose">
                 <i class="material-icons">equalizer</i>
             </div>
             <div class="card-content">
                 <p class="category">Active Users</p>
                 <h3 class="card-title">{{\App\Template::count()}}</h3>
             </div>
             <div class="card-footer">
                 <div class="stats">
                     Total number of active users
                 </div>
             </div>
         </div>
     </div>
     <div class="col-lg-3 col-md-6 col-sm-6">
         <div class="card card-stats">
             <div class="card-header" data-background-color="rose">
                 <i class="material-icons">input</i>
             </div>
             <div class="card-content">
                 <p class="category">Submissions</p>
                 <h3 class="card-title">{{\App\Submission::count()}}</h3>
             </div>
             <div class="card-footer">
                 <div class="stats">
                      Total submissions
                 </div>
             </div>
         </div>
     </div>
     <div class="col-lg-3 col-md-6 col-sm-6">
         <div class="card card-stats">
             <div class="card-header" data-background-color="rose">
                 <i class="material-icons">account_balance</i>
             </div>
             <div class="card-content">
                 <p class="category">Banks</p>
                 <h3 class="card-title">{{\App\User::roleBank()->count()}}</h3>
             </div>
             <div class="card-footer">
                 <div class="stats">
                      Total number of banks
                 </div>
             </div>
         </div>
     </div>
    </div>
</div>
</div>
@endsection



