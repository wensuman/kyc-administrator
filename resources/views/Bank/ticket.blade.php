@extends('Bank.core')

@section('contents')
	<div class="col-md-12">
	    <div class="card">
	        <div class="card-content">
	            <h3 class="card-title">Your Tickets</h3>
	             @if($tickets->count() == 0)
	            	<div class="alert alert-primary">You do not have any support ticket yet.</div>
	            @else
	            <div class="table-responsive table-bordered">
	                <table class="table">
	                    <thead >
	                        <tr><th>Name</th>
	                        <th>Title</th>
	                        <th>Status</th>
	                        <th>Action</th>
	                    </tr></thead>
	                    <tbody>
	                    @foreach($tickets as $key=> $ticket)
	                        <tr>
	                            <td>{{ucfirst($ticket->user->profile['name'] ?? $ticket->user->email)}}</td>
	                            <td>{{$ticket->ticket['title']}}</td>
	                            <td>{!!($ticket->ticket['answer'] ?? false ) ? '<i class="fa fa-check"></i>' : 'Open'!!}</td>
	                            <td > <a role="button" data-toggle="collapse" data-parent="#accordion" href='#collapse{{$key}}' aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                                    <h4 class="panel-title">
                                                        Answer   
                                                    </h4>
                                </a></td>
	                        </tr>
	                        <tr>
								<td style="padding:0;border: none" colspan="4">
								 	<div id='collapse{{$key}}' class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height:'0px'">
									    <div class="panel-body">
							    		 	<div class="col-xs-12 no-padding">
							    		 	   <div class="panel-question">
								    	
								    			{!! $ticket->ticket['detail'] ?? ''!!}
								    		   </div>
									    	</div>
									        <form action="/tickets" method="post">
									        	{{csrf_field()}}
									        	<input autocomplete="off"  name="id"  type="hidden" value="{{$ticket->id}}" />
									        	<textarea placeholder="Write your answer here..." name="answer" id="" cols="50" rows="5">{!!$ticket->ticket['answer'] ?? ''!!}</textarea>
									        	<button type="submit" class="btn btn-primary"> Post </button>
									        </form>
									   </div>
									</div>
								</td>
	                        </tr>
                        @endforeach
	                    </tbody>
	                </table>
	                </div>
	                {{$tickets->render()}}
	                @endif
	        </div>
	    </div>
	</div>
@endsection