@extends('Bank.core')

@section('contents')
<div class="card administration-card search-page">
        <div class="card-content">
        	<div class="col-sm-12">
				<h4 class="card-title">Search for - <small class="category">{{request()->route('query')}}</small> 
				</h4>
			</div>
            @if($results->count() > 0)
            @foreach($results as $result)
            <div class="col-sm-12 col-lg-6 col-md-12">
					<div class="search-profile">
		               <div class="col-sm-4">
			                    <div class="form-group">
			                        <img id="avatar" class="img-responsive" alt="Alex's Avatar" src="/profil-pic_dummy.png">
			                     </div>
			               </div>
			               <div class="col-sm-7">
			               		<h4 class="card-title">{{implode(' ',array_filter([$result->submissions['first_name'] ?? '',$result->submissions['middle_name'] ?? '',$result->submissions['last_name'] ?? '']))}}</h4>
			               		<span class="pull-left profile-info">Mobile number: </span>
			               		<span class="pull-right profile-detail">{{$result->submissions['permanent_mobile_no'] ?? 'Not Available'}}</span><br/>
								<span class="pull-left profile-info">Citizenship Id:</span>
								<span class="pull-right profile-detail">{{$result->submissions['citizenship'] ?? 'Not Available'}}</span><br/>
								<span class="pull-left profile-info">Address:</span>
								<span class="pull-right profile-detail">{{$result->submissions['permanent_municipality'] ?? ' &nbsp;'}}</span><br/>
							   <span class="pull-left profile-info">Email:</span>
								<span class="pull-right profile-detail">{{$result->submissions['permanent_email'] ?? 'Not Available'}}</span>
								<a target="_blank" href="/submissions/{{$result->id}}" class="btn btn-primary pull-left">View more</a>
			                </div>
			            </div>
            </div>
            @endforeach
            @else
            <div class="col-sm-12">
            	<div class="alert alert-primary">No Results Found.</div>
            </div>
            @endif
        </div>
        </div>
@endsection
