<style>
@if(($correspondence_address_same_as_permanent ?? 'No') == 'Yes')
    .address thead th:nth-of-type(4n), .address tbody tr td:nth-of-type(3n) {
        display: none;
    }
@endif
@if(($temporary_address_same_as_permanent ?? 'No') == 'Yes')
    .address thead th:nth-of-type(3n), .address tbody tr td:nth-of-type(2n) {
        display: none;
    }
@endif

body {
        font-family: "Open Sans", sans-serif;
        line-height: 1.25;
    }

    li {
        list-style: none;
        float: left;
    }

    li:before {
        content: '';
    }

    li:after {
        content: ',';
    }

    li:last-child:after {
        content: none;
    }

    td:first-letter {
        text-transform: capitalize;
    }

    h2 {
        text-transform: capitalize;
    }

    table {
        border: 1px solid #ccc;
        border-collapse: collapse;
        padding: 0;
        width: 98%;
        table-layout: fixed;
    }

    table caption {
        font-size: 1.5em;
    }

    table tr {
        background: #fff;
        border: 1px solid #ddd;
        padding: .35em;
    }

    table th,
    table td {
        padding: .625em;
        text-align: center;
        border-right: 1px solid #dddddd;
    }

    table th {
        font-size: .85em;
        letter-spacing: .1em;
        text-transform: uppercase;
    }

    @media screen and (max-width: 600px) {
        table {
            border: 0;
        }

        table caption {
            font-size: 1.3em;
        }

        table thead {
            border: none;
            clip: rect(0 0 0 0);
            height: 1px;
            margin: -1px;
            overflow: hidden;
            padding: 0;
            position: absolute;
            width: 1px;
        }

        table tr {
            border-bottom: 3px solid #ddd;
            display: block;
            margin-bottom: .625em;
        }

        table td {
            border-bottom: 1px solid #ddd;
            display: block;
            font-size: .8em;
            text-align: right;
        }

        table td:before {

            content: attr(data-label);
            float: left;
            font-weight: bold;
            text-transform: uppercase;
        }

        table td:last-child {
            border-bottom: 0;
        }
    }

    body {
        margin: 0;
        padding: 0;
        font-family: "Helvetica", sans-serif;
    }

    * {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .page {
        width: 210mm;
        min-height: 277mm;
        padding: 10mm;
        margin: 10mm auto;
        border: 1px rgba(128, 128, 128, 0.28) solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        page-break-after: always;
        position: relative;
    }

    .page:before {
        content: 'KYCNEPAL';
        position: absolute;
        top: 50%;
        left: calc(50% - 190px);
        color: rgba(0, 0, 0, 0.3);
        font-size: 38px;
        -ms-transform: rotate(-45deg); /* IE 9 */
        -webkit-transform: rotate(-45deg); /* Chrome, Safari, Opera */
        transform: rotate(-45deg);

    }

    .frame {
        margin: 0;
        padding: 1cm;
        border: 1px rgba(128, 128, 128, 0.1) solid;
        height: 257mm;
    }

    .nr {
        position: relative;
        margin-top: -5mm;
        float: right;
        font-size: 10px;
    }

    .card-view {
        width: 70mm;
        padding: 10px;
        font-size: 12px;
    }

    .printer {
        position: fixed;

        right: 15px;
        color: brown;
    }

    .pdf-view caption {
        margin-top: 20px;
        margin-bottom: 20px;
    }

    @page {
        size: A4;
        margin: 0;
        padding: 0;
    }

    @media print {
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }

        .printer {
            display: none;
            visibility: hidden;
        }
    }
</style>
<div><a class='printer' style="" onclick="window.print();return false;">Print</a></div>
<div class="pdf-view">    
    @include('component.pages.page-1')
    @include('component.pages.page-2')
    @if($account_number ?? false)
    @include('component.pages.page-bank')
    @endif
    @include('component.pages.page-3')
    @include('component.pages.page-4')
    @include('component.pages.page-5')
    @include('component.pages.page-6')
    @include('component.pages.page-7')
    @if(($nationality  ?? '') === 'nepalese' || ($nationality ?? '') === 'nrn')
        @if($photo_person  ?? false)
            <div class="page">
                <h3>Photo of the person</h3>
                <img src="{{env('API_URL').$photo_person}}">
            </div>
        @endif
        @if($has_citizenship ?? false)
            @if($photo_citizenship  ?? false)
                <div class="page">
                    <h3>Citizenship of the person</h3>
                    <img src="{{env('API_URL').$photo_citizenship}}">
                </div>
            @endif
        @endif
        @if($photo_utility_bill  ?? false)
            <div class="page">
                <h3>Utility bill</h3>
                <img src="{{env('API_URL').$photo_utility_bill}}">
            </div>
        @endif
        @if($photo_driving_liscence  ?? false)
            <h3>Driving license</h3>
            <img src="{{env('API_URL').$photo_driving_liscence}}">
        @endif
        @if($has_voters_id  ?? false)
            @if($photo_voter_id ?? false)
                <h3>Voters ID</h3>
                <img src="{{env('API_URL').$photo_voter_id}}">
            @endif
        @endif
    @elseif(($nationality ?? false) === 'foreigner' || strtolower($country ?? '') === "india" || ($is_refugee ?? '')=== 'no')
        @if($foreigner_photo_person  ?? false)
            <h3>Photo of person</h3>
            <img src="{{env('API_URL').$foreigner_photo_person}}">
        @endif
        @if($foreigner_photo_passport ?? false)
            <h3>Passport</h3>
            <img src="{{env('API_URL').$foreigner_photo_passport}}">
        @endif
        @if($foreigner_photo_visa ?? false)
            <h3>Visa</h3>
            <img src="{{env('API_URL').$foreigner_photo_visa}}">
        @endif
        @if($foreigner_photo_id_card ?? false)
            <h3>Identification card</h3>
            <img src="{{env('API_URL').$foreigner_photo_id_card}}">
        @endif
    @elseif(strtolower($country  ?? '') === 'india')
        @if($indian_photo_person  ?? false)
            @if($indian_photo_id_card  ?? false)
                <h3>Identification card</h3>
                <img src="{{env('API_URL').$indian_photo_id_card}}">
            @endif
            @if($indian_photo_valid_identification  ?? false)
                <h3>Valid identification card</h3>
                <img src="{{env('API_URL').$indian_photo_valid_identification}}">
            @endif
            @if($indian_photo_any_other_documents  ?? false)
                <h3>Other documents</h3>
                <img src="{{env('API_URL').$indian_photo_any_other_documents}}">
            @endif
        @endif
        @if($client_signature ?? false)
            <h3>Signature</h3>
            <img src="{{env('API_URL').$client_signature}}">
        @endif
        @if(!($client_signature ?? true))
            @if($client_right_thumb_image  ?? false)
                <h3>Left thumb</h3>
                <img src="{{env('API_URL').$client_right_thumb_image}}">
            @endif
            @if($client_left_thumb_image ?? false)
                <h3>Right thumb</h3>
                <img src="{{env('API_URL').$client_left_thumb_image}}">
            @endif
        @endif
    @endif
</div>

