@component('mail::message')
{{$text}}
@component('mail::button', ['url' => $url])
    Review
@endcomponent
@endcomponent