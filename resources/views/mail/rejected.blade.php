@component('mail::message')
{{$text}}
@component('mail::button', ['url' => $url])
@endcomponent
@endcomponent