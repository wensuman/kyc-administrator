@component('mail::message')
{{$text}}
@component('mail::button', ['url' => $url])
    View
@endcomponent
<br>
Thank you,
<br>
KYCNEPAL
@endcomponent