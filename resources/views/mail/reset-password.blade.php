@component('mail::message')
# Password Reset Link

Recently, we recieved the password request from your email address.

@component('mail::button', ['url' => $url])
Reset
@endcomponent

If this was not you just ignore it.

Thanks,<br>
{{ config('app.name') }}
@endcomponent