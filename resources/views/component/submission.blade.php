<div class="col-md-6">
    <div class="itemdiv commentdiv">
        <div class="body">
            <div class="name">
                <a href="#">{{$user->template->name ?? $user['profile']['name'] ?? $user['profile']['name'] ?? $user['email'] ?? 'Not available' }}</a>
            </div>
            <div class="time">
                <i class="ace-icon fa fa-clock-o"></i>
                <span class="green">{{$created_date ? \Carbon\Carbon::parse($created_date)->toFormattedDateString(): ''}}</span>
            </div>
            <div class="text">
                A new form has been submitted to {{$bank['profile']['name'] ?? 'Not Available'}}
            </div>
        </div>
    </div>
</div>