<div class="col-md-12">
    <div class="itemdiv memberdiv">
        <div class="body user-lists">
            <div class="col-sm-7">
                <div class="name">
                    {{$list->template->name ?? $list->profile['name'] ?? $email ?? $phone_number ?? ''}}
                </div>
            </div>
            <div class="col-sm-3">
                <div class="time">
                    <i class="ace-icon fa fa-clock-o"></i>
                    <span class="green">{{$created_at ?  \Carbon\Carbon::parse($created_date)->toFormattedDateString(): ''}}</span>
                </div>
            </div> 
            <div class="col-sm-2">               
                <div>
                    <span>{{$deleted_at ?? false ? 'Disabled' : 'Active'}}</span>
                </div>
            </div>
        </div>
    </div>
</div>