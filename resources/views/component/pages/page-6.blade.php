<div class="page">
    <table>
        <caption>Family Details</caption>
        <tr>
            <td>Father</td>
            <td>{{$father_name ?? ''}}</td>
        </tr>
        <tr>
            <td>Mother</td>
            <td>{{$mother_name ?? ''}}</td>
        </tr>
        <tr>
            <td>Grandfather</td>
            <td>{{$grand_father_name ?? ''}}</td>
        </tr>
        <tr>
            <td>Grandmother</td>
            <td>{{$grand_mother_name ?? ''}}</td>
        </tr>
        <tr>
            <td>Marital status</td>
            <td>{{$marital_status ?? ''}}</td>
        </tr>
        @if(strtolower($marital_status ?? '') == 'married')
            <tr>
                <td>Spouse</td>
                <td>{{$spouse_name  ?? ''}}
                </td>
            </tr>
            <tr>
                <td>Name of son's</td>
                <td class="pull-right">
                    {{ implode(', ', array_filter([$son_name_1 ?? '',$son_name_2 ?? '', $son_name_3 ?? ''])) }}
                </td>
            </tr>
            <tr>
                <td>Name of daughter's</td>
                <td class="pull-right">
                    {{implode(', ',array_filter([$daughter_name_1 ?? '', $daughter_name_2 ?? '', $daughter_name_3 ?? '']))}}
                </td>
            </tr>
            <tr>
                <td>Name of daughter in law's</td>
                <td class="pull-right">
                    {{implode(', ',array_filter([$daughter_in_law_name_1 ?? '', $daughter_in_law_name_2 ?? '', $daughter_in_law_name_3 ?? '']))}}
                </td>
            </tr>
        @endif

        @if(($gender  ?? '') ==='female' && ( $marital_status ?? '' ) === 'married')
            <tr>
                <td>Father in law</td>
                <td>{{$father_in_law ?? ''}}</td>
            </tr>
            <tr>
                <td>Mother in law</td>
                <td>{{$mother_in_law ?? ''}}</td>
            </tr>
        @endif
    </table>
</div>