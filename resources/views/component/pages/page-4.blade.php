<div class="page">
    <table>
        <caption>
            Current Residential Address
        </caption>
        <tr>
            <td>District</td>
            <td>{{$current_district_name ?? ''}}</td>
        </tr>
        <tr>
            <td>Municipality / VDC</td>
            <td>{{$current_city ?? ''}}</td>
        </tr>
        <tr>
            <td>Ward No. </td>
            <td>{{$current_ward_no ?? ''}}</td>
        </tr>
        <tr>
            <td>Town / City</td>
            <td>{{$current_town ?? ''}}</td>
        </tr>
        <tr>
            <td>Street / Tole</td>
            <td>{{$current_street ?? ''}}</td>
        </tr>
      
        <tr>
            <td>House / Apartment No.</td>
            <td>{{$current_house_no ?? ''}}</td>
        </tr>
        <tr>
            <td>Is tenant in residence?</td>
            <td>{{$is_tenant  ?? 'No'}}</td>
        </tr>
        @if(strtolower($is_tenant ?? '') === 'yes')
            <tr>
                <td>Full Name</td>
                <td>{{$landlord_name ?? ''}}</td>
            </tr>
            <tr>
                <td>Contact No. </td>
                <td>{{$landlord_contact_number ?? ''}}</td>
            </tr>
            <tr>
                <td>Email Address</td>
                <td>{{$landlord_email ?? ''}}</td>
            </tr>
        @endif
        <tr>
            <td>Is current address same as permanent address?</td>
            <td>{{$current_address_same_as_permanent ?? ''}}</td>
        </tr>
        @if(strtolower($current_address_same_as_permanent  ?? '') === 'no')
            <tr>
                <td>District</td>
                <td>{{$permanent_district_name ?? ''}}</td>
            </tr>
            <tr>
                <td>Municipality / VDC</td>
                <td>{{$permanent_municipality_name ?? ''}}</td>
            </tr>
            <tr>
                <td>Ward No.</td>
                <td>{{$permanent_ward_no ?? ''}}</td>
            </tr>
            <tr>
                <td>Town / City</td>
                <td>{{$permanent_town ?? ''}}</td>
            </tr>
            <tr>
                <td>Street / Tole</td>
                <td>{{$permanent_street ?? ''}}</td>
            </tr>
            <tr>
                <td>House / Apartment No.</td>
                <td>{{$permanent_house_number ?? ''}}</td>
            </tr>
        @endif
        <tr>
            <td>
                Is communication address different than current and permanent address?
            </td>
            <td>
                {{$current_address_different_as_communication ?? 'No'}}
            </td>
        </tr>
        @if(strtolower($current_address_different_as_communication ?? '') == 'yes')
            <tr class="template-part-detail">
                <td>Country</td>
                <td>{{$communication_country ?? ''}}</td>
            </tr>
            <tr class="template-part-detail">
                <td>Town / City / State</td>
                <td>{{$communication_town ?? ''}}</td>
            </tr>
            <tr class="template-part-detail">
                <td>Street / Tole </td>
                <td>{{$communication_street ?? ''}}</td>
            </tr>
            <tr class="template-part-detail">
                <td>House / Apartment No.</td>
                <td>{{$communication_house_number ?? ''}}</td>
            </tr>
            <tr class="template-part-detail">
                <td>Email Address</td>
                <td>{{$communication_email ?? ''}}</td>
            </tr>
            <tr class="template-part-detail">
                <td>P.O.B. No. / Postal Code</td>
                <td>{{$communication_p_o_b_no ?? ''}}</td>
            </tr>
        @endif
    </table>
</div>