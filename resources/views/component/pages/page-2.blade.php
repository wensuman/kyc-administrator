<div class="page">
    <table>
        <caption>Identity</caption>
        <tr>
            <td>First Name</td>
            <td class="pull-right">{{$first_name ?? ''}}</td>
        </tr>
        @if($middle_name ?? false)
            <tr>
                <td>Middle Name</td>
                <td class="pull-right">{{$middle_name ?? ''}}</td>
            </tr>
        @endif
        <tr>
            <td>Last Name</td>
            <td class="pull-right">{{$last_name ?? ''}}</td>
        </tr>
        <tr>
            <td>Gender</td>
            <td class="pull-right">{{$gender ?? ''}}</td>
        </tr>
        <tr>
            <td>Date of Birth</td>
            <td class="pull-right">{{$date_of_birth_en ?? ''}} A.D. / {{$date_of_birth_np ?? ''}} B.S.</td>
        </tr>
    </table>
</div>