<div class="page">
    <table>
        <caption>Occupation</caption>
        <tr>
            <td>Profession</td>
            <td>{{$profession ?? ''}}</td>
        </tr>
        <tr>
            <td>Employment</td>
            <td>{{$employment_type ?? ''}}</td>
        </tr>
        @if($other_employment ?? '')
        <tr>
            <td>Other employment</td>
            <td>{{$other_employment ?? ''}}</td>
        </tr>
        @endif
        @if(($employment_type  ?? '') === 'business')
            <tr>
                <td>Nature of business</td>
                <td>{{$nature_of_business ?? ''}}</td>
            </tr>
            @if($other_nature_of_business ?? '')
                <tr>
                    <td>Other nature of business</td>
                    <td>{{$other_nature_of_business ?? ''}}</td>
                </tr>
            @endif
            @if($pan_no_business ?? '')
            <tr>
                <td>PAN / VAT Number of business</td>
                <td>{{$pan_no_business ?? ''}}</td>
            </tr>
            @endif
        @endif
    </table>
    <br>
    <br>
    <br>
    <table class="table table-hover">
        <tbody>
        <tr>
            <td>Organization Name</td>
            <td>Address</td>
            <td>Designation</td>
            <td>Estimated Anual Income</td>
        </tr>
        @if($organization_name_1  ?? false)
            <tr>
                <td>{{$organization_name_1 ?? ''}}</td>
                <td>{{$organization_address_1 ?? ''}}</td>
                <td>{{$organization_designation_1 ?? ''}}</td>
                <td>{{$organization_estimate_annual_income_1 ?? ''}}</td>
            </tr>
        @endif
        @if($organization_name_2  ?? false)
            <tr>
                <td>{{$organization_name_2 ?? ''}}</td>
                <td>{{$organization_address_2 ?? ''}}</td>
                <td>{{$organization_designation_2 ?? ''}}</td>
                <td>{{$organization_estimate_annual_income_2 ?? ''}}</td>
            </tr>
        @endif
        @if($other_income_source  ?? false)
            <tr>
                <td>Other income source</td>
                <td colspan="2">{{$other_income_source ?? ''}}</td>
                <td>{{$other_estimated_annual_income ?? ''}}</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>