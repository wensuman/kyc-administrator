<div class="page">
    <table>
        <caption>Account</caption>
        <tr>
            <td>Account Number</td>
            <td>{{$account_number ?? ''}}</td>
        </tr>
        <tr>
            <td>Relationship with account holder</td>
            <td>{{$relationship_with_account_holder ?? ''}}</td>
        </tr>
        @if($relationship_with_account_holder_other ?? false)
            <tr>
                <td>Relationship with account holder</td>
                <td>{{$relationship_with_account_holder_other ?? ''}}</td>
            </tr>
        @endif
        <tr>
            <td>Availing card ? </td>
            <td>{{$availing_credit_card ?? 'No'}}</td>
        </tr>
        @if($availing_credit_card === 'yes')
            <tr>
                <td>Type of availing card</td>
                <td>{{$availing_credit_card ?? ''}}</td>
            </tr>
        @endif
        <tr>
            <td>Estimated monthly turnover</td>
            <td>{{$estimated_monthly_turnover ?? ''}}</td>
        </tr>
        <tr>
            <td>Estimated yearly transaction</td>
            <td>{{$estimated_yearly_transaction ?? ''}}</td>
        </tr>
        <tr>
            <td>Accounts in other bank</td>
            <td>{!!implode('<br/>', collect($accounts ?? [])->map(function($account){
                $account = (array) $account;
                return collect([$account['name'] ?? '',$account['branch'] ?? '', $account['account_type'] ?? ''])->filter()->values()->implode(', ');
            })->toArray())!!}</td>
        </tr>
    </table>
</div>

                                                    
                                                       
                                                  