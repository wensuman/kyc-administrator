<div class="page">
    <table>
        <caption>
            Miscellaneous
        </caption>
        <tr>
            <td>Religion</td>
            <td>{{$religion ?? ''}}</td>
        </tr>
        @if($other_religion ?? '')
            <tr>
                <td>Other Religion</td>
                <td>{{$other_religion ?? ''}}</td>
            </tr>
        @endif
        <tr>
            <td>Education</td>
            <td>{{$education ?? ''}}</td>
        </tr>
        <tr>
            <td>Do you have a beneficial owner?</td>
            <td>{{$has_beneficial_owner ?? ''}}</td>
        </tr>
        @if(($has_beneficial_owner ?? '') == 'yes')
            <tr>
                <td>Name of beneficial owner</td>
                <td>{{$beneficial_owner_name ?? ''}}</td>
            </tr>
            <tr>
                <td>Address of beneficial owner</td>
                <td>{{$beneficial_owner_address ?? ''}}</td>
            </tr>
            <tr>
                <td>Relation with beneficial owner</td>
                <td>{{$beneficial_owner_relation ?? ''}}</td>
            </tr>
            <tr>
                <td>Contact no. of beneficial owner</td>
                <td>{{$beneficial_owner_contact_number ?? ''}}</td>
            </tr>
        @endif
        <tr>
            <td>Source of funds</td>
            <td>{{$source_of_funds ?? ''}}</td>
        </tr>
        @if(($source_of_funds ?? '') == 'Others')
        <tr>
            <td>Other source of funds</td>
            <td>{{$other_source_of_funds}}</td>
        </tr>
        @endif
    </table>
</div>