<div class="page">
    <table>
        <caption>Nationality</caption>
        <tr>
            <td>Nationality</td>
            <td>{{$nationality ?? ''}}</td>
        </tr>
        @if(($nationality  ?? '') === 'nepalese' || ($nationality  ?? '') === 'nrn'  )
            <div>
                <tr>
                    <td>Do you have citizenship?</td>
                    <td>{{$has_citizenship ?? ''}}</td>
                </tr>
                @if(($has_citizenship ?? false ) === 'yes')
                    <tr>
                        <td>Citizenship No.</td>
                        <td>{{$citizenship_no ?? ''}}</td>
                    </tr>
                    <tr>
                        <td>Date of Issue</td>
                        <td>{{$citizenship_issued_on ?? ''}}</td>
                    </tr>
                    <tr>
                        <td>Issued District</td>
                        <td>{{$citizenship_issued_by ?? ''}}</td>
                    </tr>
                @endif
                <tr>
                    <td>Voters ID number</td>
                    <td>{{$voters_id_no ?? ''}}</td>
                </tr>
                <tr>
                    <td>Do you have passport?</td>
                    <td>{{$has_passport ?? 'No'}}</td>
                </tr>
                @if(($has_passport ?? '') === 'yes')
                    <tr>
                        <td>Passport No.</td>
                        <td>{{$passport_no ?? ''}}</td>
                    </tr>
                    <tr>
                        <td>Date of Issue</td>
                        <td>{{$passport_issued_on ?? ''}}</td>
                    </tr>
                    <tr>
                        <td>Expiry Date</td>
                        <td>{{$passport_expiry_date ?? ''}}</td>
                    </tr>
                    <tr>Unavailable
                        <td>Issued District</td>
                        <td>{{$passport_issued_district ?? ''}}</td>
                    </tr>
                @endif
            </div>
        @endif
        @if(($nationality ?? false) === 'foreigner')
            <div>
                <tr>
                    <td>Country</td>
                    <td>{{$country ?? ''}}</td>
                </tr>
                <tr>
                    <td>Are you refugee?</td>
                    <td>{{$is_refugee  ?? 'No'}}
                    </td>
                </tr>
                @if(($is_refugee ?? '') === 'yes' || ($identification_no ?? ''))
                    <tr>
                        <td>ID No.</td>
                        <td>{{$identification_no ?? ''}}</td>
                    </tr>
                    <tr>
                        <td>Id Type</td>
                        <td>{{$identification_type ?? ''}}</td>
                    </tr>
                    <tr>
                        <td>Issuing Authority</td>
                        <td>{{$identification_issued_authority ?? ''}}</td>
                    </tr>
                    <tr>
                        <td>Date of Issue</td>
                        <td>{{$identification_issued_on ?? ''}}</td>
                    </tr>
                @endif
                <tr>
                    <td>Passport Number</td>
                    <td>{{$foreign_passport_number ?? ''}}</td>
                </tr>
                <tr>
                    <td>Passport Issued on</td>
                    <td>{{$foreign_passport_issued_on ?? ''}}</td>
                </tr>
                <tr>
                    <td>Passport expiry date</td>
                    <td>{{$foreign_passport_expiry_date ?? ''}}</td>
                </tr>
            </div>
        @endif
    </table>
</div>