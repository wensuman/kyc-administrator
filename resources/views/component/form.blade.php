@section('nav')
    <a class="navbar-brand">Customer form</a>
@endsection

@section('contents')
    <div class="card displayer">
        <link rel="stylesheet" href="/css/form-dash.css">
        <div id="form-complete">
            <div class="card-content">
                <h4 class="col-sm-12 card-title">Review the following information</h4>
                <div>
                    <div class="clearfix"></div>
                    <script type="text/javascript">
                        function open_panel() {
                            slideIt();
                            var a = document.getElementById("sidebar");
                            a.setAttribute("id", "sidebar1");
                            a.setAttribute("onclick", "close_panel()");
                        }
                        function slideIt() {
                            var slidingDiv = document.getElementById("slider");
                            var stopPosition = 0;
                            if (parseInt(slidingDiv.style.right) < stopPosition) {
                                slidingDiv.style.right = parseInt(slidingDiv.style.right) + 8 + "px";
                                setTimeout(slideIt, 0.1);
                            }
                        }
                        function close_panel() {
                            slideIn();
                            a = document.getElementById("sidebar1");
                            a.setAttribute("id", "sidebar");
                            a.setAttribute("onclick", "open_panel()");
                        }
                        function slideIn() {
                            var slidingDiv = document.getElementById("slider");
                            var stopPosition = -342;
                            if (parseInt(slidingDiv.style.right) > stopPosition) {
                                slidingDiv.style.right = parseInt(slidingDiv.style.right) - 10 + "px";
                                setTimeout(slideIn, 0.1);
                            }
                        }
                    </script>
                    <div id="slider" style="right:-342px;">
                        <div id="sidebar" onclick="open_panel()">
                            <img src="/sidebar1.png" class="side-image-1"/>
                        </div>
                        <div id="header">
                            <div class="card" id="screener"
                                 style="@if(str_contains($_SERVER['SERVER_NAME'],'bank')) pointer-events:none; @endif">
                                <div class="card-content review-sidebar">
                                    <span v-html="savingText" class="saving-spinner"></span>
                                    @foreach(config('screening.types') as $screening)
                                        @if($screening['type'] == 'checkbox')
                                            <div class="col-sm-12 no-padding">
                                                <div class="checkbox checkbox-inline">
                                                    <label>
                                                        <input autocomplete="off" type="{{$screening['type']}}"
                                                               @change="submit"
                                                               v-model="state.{{str_slug($screening['name'])}}"
                                                               name="{{str_slug($screening['name'])}}"
                                                               class="constitution-details ace"> <span
                                                                v-show="{{$screening['type'] == 'checkbox'}}">{{ucfirst($screening['label'])}} </span>
                                                    </label>
                                                </div>
                                            </div>
                                        @else
                                            <div class="col-sm-12 no-padding"
                                                 v-show="state.{{str_slug($screening['if'])}}">
                                                <div class="checkbox checkbox-inline">
                                                    <label>
                                                        <input autocomplete="off" type="{{$screening['type']}}"
                                                               @keyup="update"
                                                               v-model="state.{{str_slug($screening['name'])}}"
                                                               name="{{str_slug($screening['name'])}}"
                                                               class="constitution-details ace"
                                                               placeholder="{{$screening['label']}}">
                                                    </label>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-card col-sm-12 no-padding">
                        <div class="card-form">
                            <div class="card-content customer-display">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                    Identity<i class="content pull-right"></i>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="headingOne">
                                            <div class="panel-body">
                                                <div class="col-xs-12 no-padding">
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">First Name</span>
                                                        <span class="pull-right">{{$first_name ?? ''}}</span>
                                                    </div>

                                                    @if($middle_name ?? false)
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Middle Name</span>
                                                            <span class="pull-right">{{$middle_name ?? ''}}</span>
                                                        </div>
                                                    @endif

                                                    <div class="template-part-detail">
                                                        <span class="pull-left">Last Name</span>
                                                        <span class="pull-right">{{$last_name ?? ''}}</span>
                                                    </div>
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">Gender</span>
                                                        <span class="pull-right">{{$gender ?? ''}}</span>
                                                    </div>
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">Date of Birth</span>
                                                        <?php
                                                            
                                                        ?>
                                                        <span class="pull-right">{{collect([($date_of_birth_en ?? '') ? $date_of_birth_en.' A.D. ' : '',($date_of_birth_np ?? '') ? $date_of_birth_np .' B.S. ': ''  ])->filter()->implode(' / ')}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if($account_number ?? false)
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                    href="#collapsetwo"
                                                    aria-expanded="false" aria-controls="collapsetwo">
                                                    Account<i class="content pull-right"></i>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapsetwo" class="panel-collapse collapse" role="tabpanel"
                                                aria-labelledby="headingOne">
                                            <div class="panel-body">
                                                <div class="row no-margin">
                                                    <div class="col-xs-12 no-padding">
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Account Number</span>
                                                            <span class="pull-right">{{$account_number ?? ''}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Relationship with account holder</span>
                                                            <span class="pull-right">{{$relationship_with_account_holder ?? ''}}</span>
                                                        </div>
                                                        @if($relationship_with_account_holder_other ?? false)
                                                            <div class="template-part-detail">
                                                                <span class="pull-left">Relationship with account holder</span>
                                                                <span class="pull-right">{{$relationship_with_account_holder_other ?? ''}}</span>
                                                            </div>
                                                        @endif
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Availing card ?</span>
                                                            <span class="pull-right">{{$availing_credit_card ?? 'No'}}</span>
                                                        </div>
                                                        @if($availing_credit_card === 'yes')
                                                            <div class="template-part-detail">
                                                                <span class="pull-left">Type of availing card</span>
                                                                <span class="pull-right">{{$availing_credit_card ?? ''}}</span>
                                                            </div>
                                                        @endif
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Estimated monthly turnover</span>
                                                            <span class="pull-right">{{$estimated_monthly_turnover ?? ''}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Estimated yearly transaction</span>
                                                            <span class="pull-right">{{$estimated_yearly_transaction ?? ''}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Accounts in other bank</span>
                                                            <span class="pull-right">{!!implode('<br/>', collect($accounts ?? [])->map(function($account){
                                                                $account = (array) $account;
                                                                return collect([$account['name'] ?? '',$account['branch'] ?? '', $account['account_type'] ?? ''])->filter()->values()->implode(', ');
                                                            })->toArray())!!}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapsetwo"
                                                   aria-expanded="false" aria-controls="collapsetwo">
                                                    Nationality<i class="content pull-right"></i>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapsetwo" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="headingOne">
                                            <div class="panel-body">
                                                <div class="row no-margin">
                                                    <div class="col-xs-12 no-padding">
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Nationality</span>
                                                            <span class="pull-right">{{$nationality ?? ''}}</span>
                                                        </div>
                                                        @if(($nationality  ?? '') === 'nepalese' || ($nationality  ?? '') === 'nrn'  )
                                                            <div>
                                                                <div class="template-part-detail">
                                                                    <span class="pull-left">Has citizenship?</span>
                                                                    <span class="pull-right">{{$has_citizenship ?? ''}}</span>
                                                                </div>
                                                                @if(($has_citizenship ?? false ) === 'yes')
                                                                    <div class="template-part-detail">
                                                                        <span class="pull-left">Citizenship No.</span>
                                                                        <span class="pull-right">{{$citizenship_no ?? ''}}</span>
                                                                    </div>
                                                                    <div class="template-part-detail">
                                                                        <span class="pull-left">Date of Issue</span>
                                                                        <span class="pull-right">{{$citizenship_issued_on ?? ''}}</span>
                                                                    </div>
                                                                    <div class="template-part-detail">
                                                                        <span class="pull-left">Issued District</span>
                                                                        <span class="pull-right">{{$citizenship_issued_district ?? ''}}</span>
                                                                    </div>
                                                                @endif
                                                                <div class="template-part-detail">
                                                                    <span class="pull-left">Voters ID number</span>
                                                                    <span class="pull-right">{{$voters_id_no ?? ''}}</span>
                                                                </div>
                                                                <div class="template-part-detail">
                                                                    <span class="pull-left">Do you have a passport?</span>
                                                                    <span class="pull-right">{{$has_passport ?? ''}}</span>
                                                                </div>
                                                                @if(strtolower($has_passport ?? '') === 'yes')
                                                                    <div class="template-part-detail">
                                                                        <span class="pull-left">Passport No.</span>
                                                                        <span class="pull-right">{{$passport_no ?? ''}}</span>
                                                                    </div>
                                                                    <div class="template-part-detail">
                                                                        <span class="pull-left">Date of Issue</span>
                                                                        <span class="pull-right">{{$passport_issued_on ?? ''}}</span>
                                                                    </div>
                                                                    <div class="template-part-detail">
                                                                        <span class="pull-left">Expiry Date</span>
                                                                        <span class="pull-right">{{$passport_expiry_date ?? ''}}</span>
                                                                    </div>
                                                                    <div class="template-part-detail">
                                                                        <span class="pull-left">Issued District</span>
                                                                        <span class="pull-right">{{$passport_issued_district ?? ''}}</span>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        @endif
                                                        @if(($nationality ?? false) === 'foreigner')
                                                            <div>
                                                                <div class="template-part-detail">
                                                                    <span class="pull-left">Country</span>
                                                                    <span class="pull-right">{{$country ?? ''}}</span>
                                                                </div>
                                                                <div class="template-part-detail">
                                                                    <span class="pull-left">Are you refugee?</span>
                                                                    <span class="pull-right">{{$is_refugee  ?? ''}}
                                                                        </span>
                                                                </div>
                                                                @if(($is_refugee ?? '') === 'yes')
                                                                    <div>
                                                                        <div class="template-part-detail">
                                                                            <span class="pull-left">ID No.</span>
                                                                            <span class="pull-right">{{$identification_no ?? ''}}</span>
                                                                        </div>
                                                                        <div class="template-part-detail">
                                                                            <span class="pull-left">ID Type</span>
                                                                            <span class="pull-right">{{$identification_type ?? ''}}</span>
                                                                        </div>
                                                                        <div class="template-part-detail">
                                                                            <span class="pull-left">Issuing Authority</span>
                                                                            <span class="pull-right">{{$identification_issued_on ?? ''}}</span>
                                                                        </div>
                                                                        <div class="template-part-detail">
                                                                            <span class="pull-left">Date of Issue</span>
                                                                            <span class="pull-right">{{$identification_issued_authority ?? ''}}</span>
                                                                        </div>
                                                                        <div class="template-part-detail">
                                                                            <span class="pull-left">Expiry Date</span>
                                                                            <span class="pull-right">{{$identification_expiry_date ?? ''}}</span>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                                <div class="template-part-detail">
                                                                    <span class="pull-left">Passport No.</span>
                                                                    <span class="pull-right">{{$foreign_passport_number ?? ''}}</span>
                                                                </div>
                                                                <div class="template-part-detail">
                                                                    <span class="pull-left">Date of Issue</span>
                                                                    <span class="pull-right">{{$foreign_passport_issued_on ?? ''}}</span>
                                                                </div>
                                                                <div class="template-part-detail">
                                                                    <span class="pull-left">Expiry Date</span>
                                                                    <span class="pull-right">{{$foreign_passport_expiry_date ?? ''}}</span>
                                                                </div>
                                                                <div class="template-part-detail">
                                                                    <span class="pull-left">Individual PAN No.</span>
                                                                    <span class="pull-right">{{$pan_no ?? ''}}</span>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapse3"
                                                   aria-expanded="false" aria-controls="collapse3">
                                                    Address <i class="content pull-right"></i>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse3" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="headingOne">
                                            <div class="panel-body">
                                                <div class="col-xs-12 no-padding">
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">District</span>
                                                        <span class="pull-right">{{$current_district_name ?? ''}}</span>
                                                    </div>
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">Municipality / VDC</span>
                                                        <span class="pull-right">{{$current_municipality_name ?? ''}}</span>
                                                    </div>
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">Town / City</span>
                                                        <span class="pull-right">{{$current_town ?? ''}}</span>
                                                    </div>
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">Ward No.</span>
                                                        <span class="pull-right">{{$current_ward_no ?? ''}}</span>
                                                    </div>
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">Street / Tole</span>
                                                        <span class="pull-right">{{$current_street ?? ''}}</span>
                                                    </div>
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">House / Apartment No.</span>
                                                        <span class="pull-right">{{$current_house_no ?? ''}}</span>
                                                    </div>
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">Are you a tenant in your current residence?</span>
                                                        <span class="pull-right">{{$is_tenant  ?? ''}}</span>
                                                    </div>
                                                    @if(($is_tenant ?? '') === 'yes')
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Full Name</span>
                                                            <span class="pull-right">{{$company_or_landlord_name ?? ''}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Contact No.</span>
                                                            <span class="pull-right">{{$company_or_landlord_number ?? ''}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Email Address</span>
                                                            <span class="pull-right">{{$company_or_landlord_email ?? ''}}</span>
                                                        </div>
                                                    @endif

                                                    <div class="template-part-detail">
                                                        <span class="pull-left">Is current address same as permanent address ( as per Citizenship Certificate )?</span>
                                                        <span class="pull-right">{{$current_address_same_as_permanent ?? ''}}</span>
                                                    </div>
                                                    @if(strtolower(($current_address_same_as_permanent  ?? '')) !== 'yes')
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">District</span>
                                                            <span class="pull-right">{{$permanent_district_name ?? ''}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Municipality / VDC</span>
                                                            <span class="pull-right">{{$permanent_municipality_name ?? ''}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Town / City</span>
                                                            <span class="pull-right">{{$permanent_town ?? ''}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Ward No.</span>
                                                            <span class="pull-right">{{$permanent_ward_no ?? ''}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Street / Tole</span>
                                                            <span class="pull-right">{{$permanent_street ?? ''}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">House / Apartment No.</span>
                                                            <span class="pull-right">{{$permanent_house_number ?? ''}}</span>
                                                        </div>
                                                    @endif
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">Is current address same as communication address?</span>
                                                        <span class="pull-right">{{$current_address_same_as_communication ?? ''}}</span>
                                                    </div>
                                                    @if(strtolower(($current_address_same_as_communication  ?? '')) !== 'yes')
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Country</span>
                                                            <span class="pull-right">{{$communication_country ?? ''}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Town / City / State</span>
                                                            <span class="pull-right">{{$communication_town ?? ''}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Street / Tole </span>
                                                            <span class="pull-right">{{$communication_street ?? ''}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">House / Apartment No.</span>
                                                            <span class="pull-right">{{$communication_house_number ?? ''}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Email Address</span>
                                                            <span class="pull-right">{{$communication_email ?? ''}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">P.O.B. No. / Postal Code</span>
                                                            <span class="pull-right">{{$communication_p_o_b_no ?? ''}}</span>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapse4"
                                                   aria-expanded="false" aria-controls="collapse4">
                                                    Occupation <i class="content pull-right"></i>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse4" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="headingOne">
                                            <div class="panel-body">
                                                <div class="col-xs-12 no-padding">
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">Profession</span>
                                                        <span class="pull-right">{{$profession ?? ''}}</span>
                                                    </div>
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">Employment</span>
                                                        <span class="pull-right">{{$employment_type ?? ''}}</span>
                                                    </div>
                                                    @if($other_employment ?? '')
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Other employment type</span>
                                                            <span class="pull-right">{{$other_employment ?? ''}}</span>
                                                        </div>
                                                    @endif
                                                    @if(($employment_type  ?? '') === 'business')
                                                        <div>
                                                            <div class="template-part-detail">
                                                                <span class="pull-left">Nature of business</span>
                                                                <span class="pull-right">{{$nature_of_business ?? ''}}</span>
                                                            </div>
                                                            @if($other_nature_of_business ?? '')
                                                            <div class="template-part-detail">
                                                                <span class="pull-left">Other Nature of business</span>
                                                                <span class="pull-right">{{$other_nature_of_business ?? ''}}</span>
                                                            </div>
                                                            @endif
                                                            <div class="template-part-detail">
                                                                <span class="pull-left">PAN / VaT  Number of business</span>
                                                                <span class="pull-right">{{$pan_no_business ?? ''}}</span>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    <div class="template-part-detail">
                                                        <table class="table table-hover table-bordered">
                                                            <tbody>
                                                            <tr>
                                                                <td>Organization Name</td>
                                                                <td>Address</td>
                                                                <td>Designation</td>
                                                                <td>Estimated Anual Income</td>
                                                            </tr>
                                                            @if($organization_name_1  ?? false)
                                                                <tr>
                                                                    <td>{{$organization_name_1 ?? ''}}</td>
                                                                    <td>{{$organization_address_1 ?? ''}}</td>
                                                                    <td>{{$organization_designation_1 ?? ''}}</td>
                                                                    <td>{{$organization_estimate_annual_income_1 ?? ''}}</td>
                                                                </tr>
                                                            @endif
                                                            @if($organization_name_2  ?? false)
                                                                <tr>
                                                                    <td>{{$organization_name_2 ?? ''}}</td>
                                                                    <td>{{$organization_address_2 ?? ''}}</td>
                                                                    <td>{{$organization_designation_2 ?? ''}}</td>
                                                                    <td>{{$organization_estimate_annual_income_2 ?? ''}}</td>
                                                                </tr>
                                                            @endif
                                                            @if($other_income_source  ?? false)
                                                                <tr>
                                                                    <td>Other income source</td>
                                                                    <td colspan="2">{{$other_income_source ?? ''}}</td>
                                                                    <td>{{$other_estimated_annual_income ?? ''}}</td>
                                                                </tr>
                                                            @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapse5"
                                                   aria-expanded="false" aria-controls="collapse5">
                                                    Family Details <i class="content pull-right"></i>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse5" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="headingOne">
                                            <div class="panel-body">
                                                <div class="row no-margin">
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">Father Name</span>
                                                        <span class="pull-right">{{$father_name ?? ''}}</span>
                                                    </div>
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">Mother Name</span>
                                                        <span class="pull-right">{{$mother_name ?? ''}}</span>
                                                    </div>
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">Grandfather Name</span>
                                                        <span class="pull-right">{{$grand_father_name ?? ''}}</span>
                                                    </div>
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">Grandmother Name</span>
                                                        <span class="pull-right">{{$grand_mother_name ?? ''}}</span>
                                                    </div>
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">Marital status</span>
                                                        <span class="pull-right">{{$marital_status ?? ''}}</span>
                                                    </div>
                                                    @if(($marital_status ?? '' )=== 'married')
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Spouse Name</span>
                                                            <span class="pull-right">{{$spouse_name  ?? ''}}
                                                           </span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Name of son's</span>
                                                            <span class="pull-right">{{collect([$son_name_1 ?? '',$son_name_2 ?? '',$son_name_3 ?? '',])->filter()->implode(', ')}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Name of daughter's</span>
                                                            <span class="pull-right">{{collect([$daughter_name_1 ?? '',$daughter_name_2 ?? '',$daughter_name_3 ?? '',])->filter()->implode(', ')}}</span>
                                                        </div>
                                                    @endif


                                                    @if(($gender ?? '') ==='female' && ( $marital_status ?? '' ) === 'married')
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Name of father in law</span>
                                                            <span class="pull-right">{{$father_in_law ?? ''}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Name of mother in law</span>
                                                            <span class="pull-right">{{$mother_in_law ?? ''}}</span>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapse6"
                                                   aria-expanded="false" aria-controls="collapse6">
                                                    Miscellaneous <i class="content pull-right"></i>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse6" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="headingOne">
                                            <div class="panel-body">
                                                <div class="row no-margin">
                                                    <div class="template-part-detail">
                                                            <span class="pull-left">Religion</span>
                                                            <span class="pull-right">{{$religion ?? ''}}</span>
                                                        </div>
                                                    @if($other_religion ?? '')
                                                    <div class="template-part-detail">
                                                            <span class="pull-left">Other Religion</span>
                                                            <span class="pull-right">{{$other_religion ?? ''}}</span>
                                                        </div>
                                                    @endif
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">Education</span>
                                                        <span class="pull-right">{{$education ?? ''}}</span>
                                                    </div>
                                                    <div class="template-part-detail">
                                                        <span class="pull-left">Has beneficial owner?</span>
                                                        <span class="pull-right">{{$has_beneficial_owner ?? ''}}</span>
                                                    </div>
                                                    @if(($has_beneficial_owner ?? '') == 'yes')
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Name of beneficial owner</span>
                                                            <span class="pull-right">{{$beneficial_owner_name ?? ''}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Address of beneficial owner</span>
                                                            <span class="pull-right">{{$beneficial_owner_address ?? ''}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Relation with beneficial owner</span>
                                                            <span class="pull-right">{{$beneficial_owner_relation ?? ''}}</span>
                                                        </div>
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Contact no. of beneficial owner</span>
                                                            <span class="pull-right">{{$beneficial_owner_contact_number ?? ''}}</span>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapse7"
                                                   aria-expanded="false" aria-controls="collapse7">
                                                    Documents <i class="content pull-right"></i>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse7" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="headingOne">
                                            <div class="panel-body">
                                                <div class="row no-margin">
                                                    <div class="col-xs-12 no-padding">
                                                        @if(($nationality  ?? '') === 'nepalese' || ($nationality ?? '') === 'nrn')
                                                            <div class="template-part-detail">
                                                                <span class="pull-left">Photo</span>
                                                                <span class="pull-right">
                                                                    @if($photo_person  ?? false)
                                                                        <a href="{{env('API_URL').$photo_person}}">View</a>
                                                                    @else
                                                                        Not Uploaded
                                                                    @endif
                                                                </span>
                                                            </div>
                                                            @if($has_citizenship ?? false)
                                                                <div class="template-part-detail">
                                                                    <span class="pull-left">Citizenship</span>
                                                                    <span class="pull-right">
                                                                        @if($photo_citizenship  ?? false)
                                                                            <a href="{{env('API_URL').$photo_citizenship}}">View</a>
                                                                        @else
                                                                            Not Uploaded
                                                                        @endif
                                                                    </span>
                                                                </div>
                                                            @endif
                                                            <div class="template-part-detail">
                                                                <span class="pull-left">Utility Bill</span>
                                                                <span class="pull-right">
                                                                    @if($photo_utility_bill  ?? false)
                                                                        <a href="{{env('API_URL').$photo_utility_bill}}">View</a>
                                                                    @else
                                                                        Not Uploaded
                                                                    @endif
                                                                </span>
                                                            </div>
                                                            @if($photo_driving_liscence  ?? false)
                                                                <div class="template-part-detail">
                                                                    <span class="pull-left">Driving License</span>
                                                                    <span class="pull-right">
                                                                        @if($photo_driving_liscence  ?? false)
                                                                            <a href={{env('API_URL').$photo_driving_liscence}}>View</a>
                                                                        @else
                                                                            Not Uploaded
                                                                        @endif
                                                                    </span>
                                                                </div>
                                                            @endif
                                                            @if($has_voters_id  ?? false)
                                                                <div class="template-part-detail">
                                                                    <span class="pull-left">Voters Identification</span>
                                                                    <span class="pull-right">
                                                                        @if($photo_voter_id ?? false)
                                                                            <a href="{{env('API_URL').$photo_voter_id}}">View</a>
                                                                        @else
                                                                            Not Uploaded
                                                                        @endif
                                                                    </span>
                                                                </div>
                                                            @endif
                                                        @elseif(($nationality ?? false) === 'foreigner' || strtolower($country ?? '') === "india" || ($is_refugee ?? '')=== 'no')
                                                            <div class="template-part-detail">
                                                                <span class="pull-left">Photo</span>
                                                                <span class="pull-right">
                                                                    @if($foreigner_photo_person  ?? false)
                                                                        <a href="{{env('API_URL').$foreigner_photo_person}}">View</a>
                                                                    @else
                                                                        Not Uploaded
                                                                    @endif

                                                                        </span>
                                                            </div>
                                                            <div class="template-part-detail">
                                                                <span class="pull-left">Passport</span>
                                                                <span class="pull-right">
                                                                    @if($foreigner_photo_passport ?? false)
                                                                        <a href="{{env('API_URL').$foreigner_photo_passport}}">View</a>
                                                                    @else
                                                                        Not Uploaded
                                                                    @endif
                                                                    </span>
                                                            </div>
                                                            <div class="template-part-detail">
                                                                <span class="pull-left">Visa</span>
                                                                <span class="pull-right">
                                                                    @if($foreigner_photo_visa ?? false)
                                                                        <a href="{{env('API_URL').$foreigner_photo_visa}}">View</a>
                                                                    @else
                                                                        Not Uploaded
                                                                    @endif
                                                                </span>
                                                            </div>
                                                            <div class="template-part-detail">
                                                                <span class="pull-left">Identification</span>
                                                                <span class="pull-right">
                                                                    @if($foreigner_photo_id_card ?? false)
                                                                        <a href="{{env('API_URL').$foreigner_photo_id_card}}">View</a>
                                                                    @else
                                                                        Not Uploaded
                                                                    @endif
                                                                </span>
                                                            </div>
                                                        @elseif(strtolower($country  ?? '') === 'india')
                                                            <div class="template-part-detail">
                                                                <span class="pull-left">Photo</span>
                                                                <span class="pull-right">
                                                                    @if($indian_photo_person  ?? false)
                                                                        <a href="{{env('API_URL').$indian_photo_person}}">View</a>
                                                                    @else
                                                                        Not Uploaded
                                                                    @endif
                                                                </span>
                                                            </div>
                                                            <div class="template-part-detail">
                                                                <span class="pull-left">Identification</span>
                                                                <span class="pull-right">
                                                                    @if($indian_photo_id_card  ?? false)
                                                                        <a href="{{env('API_URL').$indian_photo_id_card}}">View</a>
                                                                    @else
                                                                        Not Uploaded
                                                                    @endif
                                                                </span>
                                                            </div>
                                                            <div class="template-part-detail">
                                                                <span class="pull-left">Identification Photo</span>
                                                                <span class="pull-right">
                                                                    @if($indian_photo_valid_identification  ?? false)
                                                                        <a href="{{env('API_URL').$indian_photo_valid_identification}}">View</a>
                                                                    @else
                                                                        Not Uploaded
                                                                    @endif
                                                                </span>
                                                            </div>
                                                            <div class="template-part-detail">
                                                                <span class="pull-left">Additional Document</span>
                                                                <span class="pull-right">
                                                                    @if($indian_photo_any_other_documents  ?? false)
                                                                        <a href="{{env('API_URL').$indian_photo_any_other_documents}}">View</a>
                                                                    @else
                                                                        Not Uploaded
                                                                    @endif
                                                                </span>
                                                            </div>
                                                        @endif
                                                        <div class="template-part-detail">
                                                            <span class="pull-left">Signature</span>
                                                            <span class="pull-right">
                                                                @if($client_signature ?? false)
                                                                    <a href="{{env('API_URL').$client_signature}}">View</a>
                                                                @else
                                                                    Not Uploaded
                                                                @endif
                                                            </span>
                                                        </div>
                                                            <div class="template-part-detail">
                                                                <span class="pull-left">Right Thumb</span>
                                                                <span class="pull-right">
                                                                    @if($client_right_thumb_image  ?? false)
                                                                        <a href="{{env('API_URL').$client_right_thumb_image}}">View</a>
                                                                    @else
                                                                        Not Uploaded
                                                                    @endif
                                                                </span>
                                                            </div>
                                                            <div class="template-part-detail">
                                                                <span class="pull-left">Left Thumb</span>
                                                                <span class="pull-right">
                                                                    @if($client_left_thumb_image ?? false)
                                                                        <a href="{{env('API_URL').$client_left_thumb_image}}">View</a>
                                                                    @else
                                                                        Not Uploaded
                                                                    @endif
                                                                </span>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     
                </div>
               
            </div>
           
        </div>
    </div>
    <div class="col-sm-11 text-center">
        <a class="printer btn btn-danger respond">Report</a>
        <a class="btn btn-primary" href="/{{str_contains($_SERVER['SERVER_NAME'], 'admin')?'template' :'submissions'}}/{{$submission->id}}/download">Download</a>
    </div>


@endsection

@section('script')
    @parent

    <script defer>
        var vmu = new Vue({
            el: '#screener',
            methods: {
                submit: function () {
                    @if(str_contains($_SERVER['SERVER_NAME'],'admin'))
                    var th = this;
                    setTimeout(this.updateScreening, 300)
                    @endif
                },
                update: function () {
                    axios.post('/update-screening/{{$submission->user_id}}', this.state)
                },
                @if(str_contains($_SERVER['SERVER_NAME'],'admin'))
                updateScreening: function () {
                    axios.post('/update-screening/{{$submission->user_id}}', this.state).then(this.changeSaveStatus)
                },
                @endif
                changeSaveStatus: function () {
                   
                }
            },
            data: {
                state:{!!str_replace('"false"', "false", str_replace('"true"','true',json_encode($submission->user->screening ?? array_map(function($map){return null;},array_flip(array_pluck(config('screening.types'),'name'))))))!!},
                savingText: undefined
            },
            watch: {
                state: function () {
                    console.log('watch')
                }
            }
        })
        jQuery(document).ready(function () {
            jQuery(document).on('click', '.respond', function () {
                (new PNotify({
                    title: 'Please specify your reason',
                    addclass: "stack-site",
                    icon: false,
                    hide: false,
                    styling: 'bootstrap3',
                    confirm: {
                        prompt: true,
                        prompt_multi_line: true,
                        buttons: [{
                            text: 'Accept',
                            addClass: 'responder acceptable',
                            click: function (notice, val) {
                                jQuery('.responder').attr('disabled','disabled')
                                jQuery('.acceptable').html("<i class='fa fa-spin fa-spinner'></i>")
                                jQuery.post('/verify-{{str_contains($_SERVER['SERVER_NAME'], 'admin') ?'template':'submission'}}/{{$submission->id}}', {verified: 'verified'}).then(function () {
                                    notice.cancelRemove().update({
                                        title: 'Form is accepted.',
                                        text: '',
                                        icon: false,
                                        hide: true,
                                        type: 'success',
                                        confirm: {
                                            prompt: false
                                        },
                                        buttons: {
                                            closer: false,
                                        }
                                    });
                                }).always(function(){
                                    jQuery('.responder').removeAttr('disabled')
                                    jQuery('.acceptable').html("Accept")
                                })
                            }
                        }, {
                            text: 'Decline',
                            addClass: 'responder rejectable',
                            click: function (notice, val) {
                                if (!val) {
                                    alert('Reason is needed for rejection.', 'error');
                                    return;
                                }
                                jQuery('.responder').attr('disabled','disabled')
                                jQuery('.rejectable').attr('disabled','disabled').html("<i class='fa fa-spin fa-spinner'></i>")
                                jQuery.post('/verify-{{str_contains($_SERVER['SERVER_NAME'], 'admin') ?'template':'submission'}}/{{$submission->id}}', {
                                    verified: 'rejected',
                                    reason: val
                                }).then(function () {
                                    setTimeout(function () {
                                        window.location.reload()
                                    }, 3000)
                                    notice.cancelRemove().update({
                                        title: '{{strpos($_SERVER['SERVER_NAME'], 'admin') ? 'Form' : 'Submission'}} is declined.',
                                        text: '',
                                        icon: false,
                                        type: 'success',
                                        hide: true,
                                        confirm: {
                                            prompt: false
                                        },
                                        buttons: {
                                            closer: true,
                                        }
                                    });
                                }).always(function(){
                                    jQuery('.responder').removeAttr('disabled')
                                    jQuery('.rejectable').html("Decline")
                                })
                            }
                        }
                        ]
                    },
                    buttons: {
                        closer: false,
                    },
                    history: {
                        history: false
                    }
                })).get()
            });
        })
        // })
    </script>
@endsection